package Modelo;

import Costos.ICosto;

public class Cliente {

//<editor-fold defaultstate="collapsed" desc="** ATRIBUTOS **">
    private static int ultId;
    private int id;
    private String mail;
    private String nombreCompleto;
    private String documento;
    private ICosto costo; //Strategy
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ICosto getCosto() {
        return costo;
    }

    public void setCosto(ICosto costo) {
        this.costo = costo;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public Cliente() {
    }

    public Cliente(String mail, String nombreCompleto, String documento) {
        //this.id = ++ultId;
        this.mail = mail;
        this.nombreCompleto = nombreCompleto;
        this.documento = documento;
    }
//</editor-fold>
}
