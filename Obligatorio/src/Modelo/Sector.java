package Modelo;

import Excepciones.AtencionException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import persistencia.BaseDatos;
import persistencia.MapeadorAtencion;
import persistencia.MapeadorPuesto;
import persistencia.MapeadorSector;
import persistencia.Persistencia;

public class Sector {

//<editor-fold defaultstate="collapsed" desc="** ATRIBUTOS **">
    private int id;
    //private static int ultId;
    private Area area;
    private String nombre;
    private int ultNumeroPuesto;
    private int ultNroAtencion;
    private ArrayList<Puesto> puestos;
    private PriorityQueue<Atencion> listaEspera;
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">
    public int getId() {
        return this.id;
    }
    
    public void setId(int id){
        this.id = id;
    }

    public Area getArea() {
        
        return area;
    }
    
    public void setArea(Area area){
        this.area = area;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Puesto> getPuestos() {
        return this.puestos;
    }
    
    public void setPuestos(ArrayList<Puesto> puestos){
        this.puestos = puestos;
    }

    public int getUltNumeroPuesto() {
        return ultNumeroPuesto;
    }
    
    public void setUltNumeroPuesto(int ultNroPuesto){
        this.ultNumeroPuesto = ultNroPuesto;
    }

    public int getUltNroAtencion() {
        return ultNroAtencion;
    }

    public void setUltNroAtencion(int ultNroAtencion) {
        this.ultNroAtencion = ultNroAtencion;
    }

    public PriorityQueue<Atencion> getListaEspera() {
        return listaEspera;
    }
    
    public void setListaEspera(PriorityQueue<Atencion> listaEspera){
        this.listaEspera = listaEspera;
    }
//</editor-fold>    

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public Sector() {
        this.listaEspera = new PriorityQueue<>();
        this.puestos = new ArrayList<>();
    }

    public Sector(String nombre, Area area) {
        this.nombre = nombre;
        this.area = area;
        this.listaEspera = new PriorityQueue<>();
        this.puestos = new ArrayList<>();
    }

    public int generarNuevoNroPuesto() {
        return ++ultNumeroPuesto;
    }

    public void agregarPuesto(Puesto p) throws AtencionException {
        if (this.puestos != null) {
            this.puestos.add(p);
            p.guardarPuesto();
            this.guardarSector();
        }
    }

    public Atencion doyNumero(Cliente cli) throws AtencionException {
        this.ultNroAtencion = this.ultNroAtencion + 1;
        Atencion atencion = new Atencion(cli, this.ultNroAtencion);
        this.guardarSector();

        Puesto puestoLibre = this.puestoLibre();

        if (puestoLibre == null) {
            ponerEnEspera(atencion);
        } else {
            puestoLibre.asignoAtencion(atencion);
        }

        return atencion;
    }

    private void ponerEnEspera(Atencion a) throws AtencionException {
        this.listaEspera.add(a);//Encolar
        a.setEstado(Atencion.EstadoAtencion.EN_ESPERA);
        a.guardarAtencion();
    }

    public void asignarPuesto(Trabajador trabajador, Puesto puesto) throws AtencionException {

        //Cuando asingo un trabajador a un puesto, enseguida le busco 
        //una atencion en espera, si hay se la asgino, sino espera a nuevas atenciones.
        Puesto aux = this.buscarPuesto(puesto.getId());
        puesto.setId(aux.getId());
        puesto.setNroPuesto(aux.getNroPuesto());
        puesto.setNumerosAsignados(aux.getNumerosAsignados());
        puesto.setSector(aux.getSector());
        
        puesto.iniciarPuesto(trabajador);

        if (this.hayNroEnEspera()) {
            Atencion a = this.siguienteEnEspera();
            puesto.asignoAtencion(a);
        }
        this.guardarSector();
        puesto.guardarPuesto();
    }

    public Atencion siguienteEnEspera() {

        if (this.listaEspera != null && !this.listaEspera.isEmpty()) {
            return this.listaEspera.poll(); //Desencolar
        }
        return null;
    }

    public Puesto[] puestosLibres() throws AtencionException {
//        this.cargarPuestos();
        ArrayList<Puesto> ret = new ArrayList<>();
        for (Puesto puesto : puestos) {
            if (!puesto.estaTrabajando()) {
                ret.add(puesto);
            }
        }
        Puesto[] aux = ret.toArray(new Puesto[ret.size()]);
        return aux;
    }

    public Puesto puestoLibre() throws AtencionException {
//        this.cargarPuestos();
        for (Puesto puesto : puestos) {
            if (puesto.getAtencionActual() == null && puesto.getTrabajador() != null) {
                return puesto;
            }
        }
        return null;
    }

    public boolean hayNroEnEspera() {
        return !this.listaEspera.isEmpty();
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public boolean estaTrabajando(Trabajador t) throws AtencionException {
//        this.cargarPuestos();
        return puestos.stream().map((puesto) -> t.equals(puesto.getTrabajador())).anyMatch((esta) -> (esta));
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sector other = (Sector) obj;

        return this.id == other.id;
    }

    public ArrayList<Puesto> puestosDerivacion() throws AtencionException {
        ArrayList<Puesto> libres = new ArrayList<>();
        ArrayList<Puesto> ocupados = new ArrayList<>();
//        this.cargarPuestos();

        for (Puesto puesto : this.puestos) {
            if (puesto.estaTrabajando()) {
                if (puesto.estaLibre()) {
                    libres.add(puesto);
                } else {
                    ocupados.add(puesto);
                }
            }
        }

        //Ordena la lista ascendente por cantidad de derivados
        libres.sort((p1, p2) -> p1.cantNroDerivados() - p2.cantNroDerivados());
        ocupados.sort((p1, p2) -> p1.cantNroDerivados() - p2.cantNroDerivados());

        libres.addAll(ocupados);

        return libres;
    }
    
    public void guardarSector() throws AtencionException{
        MapeadorSector map = new MapeadorSector(this);
        BaseDatos bd = BaseDatos.getInstancia();

        try{
            bd.conectarse();
            Persistencia.getInstancia().guardar(map, "Sector");
            //bd.desconectar();
        }
        catch(SQLException e){
            throw new AtencionException(999, "Error de SQL");
        }
        finally{
            bd.desconectar();
        }
    }
    
    public Puesto buscarPuesto(int idPuesto) throws AtencionException {
//        this.cargarPuestos();
        for (Puesto p : puestos) {
            if (p.getId() == idPuesto) {
                return p;
            }
        }
        return null;
    }

//    public void cargarPuestos() throws AtencionException{
//        MapeadorPuesto map = new MapeadorPuesto();
//        String filtro = "idSector = " + this.getId();
//        BaseDatos bd = BaseDatos.getInstancia();
//
//        try{
//            bd.conectarse();
//            ArrayList<Puesto> aux = (ArrayList<Puesto>)(List)Persistencia.getInstancia().buscar(map, filtro);
//            //bd.desconectar();
//            for(Puesto puesto : aux){
//                puesto.setSector(this);
//                boolean esta = false;
//                int i = 0;
//                while (!esta && i < this.puestos.size()){
//                    puesto.setSector(this);
//                    Puesto puesto2 = this.puestos.get(i);
//                    if(puesto.getId() == puesto2.getId()){
//                        esta = true;
//                        if (puesto.getAtencionActual() != null) puesto.getAtencionActual().setPuesto(puesto2);
//                        puesto2.setNroPuesto(puesto.getNroPuesto());
//                        puesto2.setNumerosAsignados(puesto.getNumerosAsignados());
//                        puesto2.setTrabajador(puesto.getTrabajador());
//                        puesto2.setSector(puesto.getSector());
//                        puesto2.setAtencionActual(puesto.getAtencionActual());
//                    }
//                    i++;
//                }
//                if (!esta){
//                    this.puestos.add(puesto);
//                }
//            }
//        }
//        catch(SQLException e){
//            throw new AtencionException(999, "Error de SQL");
//        }
//        finally{
//            bd.desconectar();
//        }
//    }
    
    public void actualizarDatos(Persistencia inst, BaseDatos bd, SistemaPersonas sistemaPersonas) throws AtencionException{
        MapeadorPuesto mapP = new MapeadorPuesto();
        String filtro = " idSector = " + this.getId();
        ArrayList<Puesto> puestosBD = (ArrayList<Puesto>)(List)inst.buscar(mapP, filtro);

        for(Puesto puesto : puestosBD){
            puesto.setSector(this);
            if (puesto.getTrabajador() != null && puesto.getTrabajador().getId() > 0){
                Trabajador trab = sistemaPersonas.buscarTrabajador(puesto.getTrabajador().getId());
                trab.setSector(this);
                puesto.setTrabajador(trab);
            }
            else{
                puesto.setTrabajador(null);
            }
            if (puesto.getAtencionActual() != null && puesto.getAtencionActual().getId() > 0){
                puesto.actualizarDatos(inst, bd, sistemaPersonas);
            }
            else{
                puesto.setAtencion(null);
            }
            boolean esta = false;
            int i = 0;
            while (!esta && i < this.puestos.size()){
                if(puesto.getId() == this.puestos.get(i).getId()){
                    esta = true;
                    this.puestos.get(i).setSector(this);
                    this.puestos.get(i).setNroPuesto(puesto.getNroPuesto());
                    this.puestos.get(i).setNumerosAsignados(puesto.getNumerosAsignados());
                    this.puestos.get(i).setTrabajador(puesto.getTrabajador());
                    this.puestos.get(i).setAtencionActual(puesto.getAtencionActual());
                }
                i++;
            }
            if (!esta){
                this.puestos.add(puesto);
            }
        }
    }

//</editor-fold> 
}
