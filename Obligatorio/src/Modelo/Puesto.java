package Modelo;

import Excepciones.AtencionException;
import Modelo.DTO.AtencionDTO;
import java.sql.SQLException;
import java.util.Date;
import java.util.Observable;
import java.util.PriorityQueue;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import persistencia.BaseDatos;
import persistencia.MapeadorAtencion;
import persistencia.MapeadorPuesto;
import persistencia.Persistencia;

public class Puesto extends Observable {

//<editor-fold defaultstate="collapsed" desc="** ATRIBUTOS **">
    private int nroPuesto;
    private Trabajador trabajador;
    private Sector sector;
    private Atencion atencionActual;
    private final PriorityQueue<Atencion> derivados;

    //Para las estadisticas
    private int numerosAsignados;
    private Duration tiempo;

    //Para el equals
    private int id;
    //private static int ultid;

//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNroPuesto() {
        return this.nroPuesto;
    }

    public void setNroPuesto(int nroPuesto) {
        this.nroPuesto = nroPuesto;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public Atencion getAtencionActual() {
        return atencionActual;
    }

    public void setAtencion(Atencion atencion) {
        this.atencionActual = atencion;
    }

    public void setAtencionActual(Atencion atencionActual) {
        this.atencionActual = atencionActual;
    }

    public PriorityQueue<Atencion> getDerivados() {
        return derivados;
    }

    public int getNumerosAsignados() {
        return numerosAsignados;
    }

    public void setNumerosAsignados(int numerosAsignados) {
        this.numerosAsignados = numerosAsignados;
    }

    public Duration getTiempo() {
        return this.tiempo;
    }

    public void setTiempo(Duration tiempo) {
        this.tiempo = tiempo;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public Puesto() {
        this.derivados = new PriorityQueue<>();
    }

    public Puesto(int nroPuesto, Sector s) {
        this.nroPuesto = nroPuesto;
        this.sector = s;
        this.derivados = new PriorityQueue<>();
        this.tiempo = new Duration(0);
        //this.id = ++ultid;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Puesto other = (Puesto) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public boolean estaLibre() {
        return this.atencionActual == null;
    }

    public boolean estaTrabajando() {
        return this.trabajador != null;
    }

    public void quitarAtencion(boolean pedir) throws AtencionException {
        this.atencionActual = null;
        if (pedir) {
            this.pedirAtencion();
        } else {
            this.notificarPuestoVacio();
        }
    }

    public void logout() throws AtencionException {
        boolean tieneDerivados = !this.derivados.isEmpty();
        boolean estaAtendiendo = this.atencionActual != null;
        if (!tieneDerivados && !estaAtendiendo) {
            this.liberarPuesto();
        } else if (tieneDerivados) {
            throw new AtencionException(302, "Tiene atenciones derivadas.");
        } else if (estaAtendiendo) {
            throw new AtencionException(303, "Tiene una atencion en curso.");
        }
    }

    public void asignoAtencion(Atencion atencion) throws AtencionException {

        //Asginacion de referencia doble
        this.atencionActual = atencion;
        atencion.setPuesto(this);

        //Notifica a los observers del area.
        notifiarArea(atencion);

        //this.numerosAsignados++;
        this.atencionActual.setEstado(Atencion.EstadoAtencion.ASIGNADA);
        this.atencionActual.guardarAtencion();
        this.guardarPuesto();

        //Las notificaciones pasan siempre por aca.
        //Notifica a los observers del puesto
        notificarPuesto(atencion);
    }

    private void liberarPuesto() throws AtencionException {
        this.trabajador = null;
        this.atencionActual = null; //Se puede sacar
        this.guardarPuesto();
    }

    private void notifiarArea(Atencion a) {
        this.getSector().getArea().avisarAtencionAsignado(atencionActual);
    }

    private void notificarPuesto(Atencion a) {
        AtencionDTO dto = new AtencionDTO();
        dto.setAtencion(a);
        dto.setTipo(AtencionDTO.Tipo.ATENCION);
        this.setChanged();
        this.notifyObservers(dto);
    }

    public void finalizoAtencion(String descripcion, boolean pedir) throws AtencionException {

        //Atencion que se esta atendiendo
        Atencion actual = this.atencionActual;

        if (actual == null) {
            throw new AtencionException(304, "El puesto no tiene una atencion asignada");
        }

        boolean puedeFinalizar = actual.getEstado() == Atencion.EstadoAtencion.ABIERTA;

        if (!puedeFinalizar) {
            throw new AtencionException(305, "La atencion no se puede finalizar porque no esta abierta");
        }
        //Llego aca puede finalizar
        actual.finalizoAtencion(trabajador, descripcion);
        actual.guardarAtencion();
        this.guardarPuesto();

        //Libero el puesto
        this.quitarAtencion(pedir);

    }

    public void pedirAtencion() throws AtencionException {

        //Pido siguiente atencion, sino encuentra es null
        Atencion a = this.pedirSiguienteAtencion();

        if (a != null) {
            this.asignoAtencion(a);
        } else {
            this.notificarPuestoVacio();
        }
    }

    public String nombreSectorPertenece() {
        return this.getSector().getNombre();
    }

    public String nombreAreaPertenece() {
        return this.getSector().getArea().getNombre();
    }

    public void iniciarPuesto(Trabajador trabajador) throws AtencionException {
        this.trabajador = trabajador;
        this.guardarPuesto();
    }

    @Override
    public String toString() {
        return "Puesto: " + nroPuesto;
    }

    public void iniciarAtencion() throws AtencionException {

        if (this.atencionActual == null) {
            throw new AtencionException(306, "El puesto no tiene atencion asignada");
        }
        if (this.atencionActual.getEstado() != Atencion.EstadoAtencion.ASIGNADA) {
            throw new AtencionException(307, "La atencion ya se abrio");
        }

        this.atencionActual.inicioAtencion(this);
        this.numerosAsignados++;
        this.guardarPuesto();
        this.notificarPuesto(atencionActual);

    }

    public void agregarTiempoAtencion(Date inicioD, Date finD) throws AtencionException {

        DateTime inicio = new DateTime(inicioD);
        DateTime fin = new DateTime(finD);

        //Diferencia del tiempo creada la atencion al tiempo que se incicio
        Duration d = new Duration(inicio, fin);

        //A la duracion que tenia le agrego la nueva
        this.tiempo = this.tiempo.plus(d);
        this.guardarPuesto();
    }

    public String TiempoPromedioStr() {
        if (numerosAsignados == 0) {
            return 0 + "";
        }
        //Paso duracion a milisegundos
        long ms = this.tiempo.getMillis();
        ms /= numerosAsignados;
        //Promedio milisegundos convierto a duration.
        Duration aux = new Duration(ms);
        //Armo string.
        String ret = aux.getStandardHours() + " Hrs-" + aux.getStandardMinutes() + " Min-" + aux.getStandardSeconds() + " Sg";
        return ret;
    }

    public int cantNroDerivados() {
        return this.derivados.size();
    }

    private Atencion pedirSiguienteAtencion() {
        //Busco atencion para ser atendida
        if (this.derivados.isEmpty()) {
            //Voy a buscar al sector si no tiene derivados
            return this.getSector().siguienteEnEspera();
        } else {
            //Desencolar de los derivados
            return this.derivados.poll();
        }
    }

    private void notificarPuestoVacio() throws AtencionException {
        this.guardarPuesto();
        this.notificarPuesto(null);
    }

    public void recibirDerivacion(AtencionDTO a) throws AtencionException {
        if (a != null) {
            a.getAtencion().setEstado(Atencion.EstadoAtencion.DERIVADA);
            if (this.atencionActual == null) {
                this.asignoAtencion(a.getAtencion());
            } else {
                this.derivados.add(a.getAtencion());
                a.getAtencion().setPuesto(this);
                this.notificarDerivacion(a);
                a.getAtencion().guardarAtencion();
            }
        } else {
            throw new AtencionException(311, "No se puede agregar una atencion null a los derivados");
        }
    }

    public void preguntarDerivacion(AtencionDTO dto) {
        //Le notifico al puesto la atencion derivada
        dto.getAtencion().setEstado(Atencion.EstadoAtencion.DERIVADA);
        dto.setPregunta(true);
        dto.setUltimoPuesto(false);
        dto.setTipo(AtencionDTO.Tipo.DERIVACION);
        notificarDerivacion(dto);
    }

    public void notificarDerivacion(AtencionDTO dto) {
        dto.setTipo(AtencionDTO.Tipo.DERIVACION);
        this.setChanged();
        this.notifyObservers(dto);
    }

    public void guardarPuesto() throws AtencionException {
        MapeadorPuesto map = new MapeadorPuesto(this);
        BaseDatos bd = BaseDatos.getInstancia();
        try {
            bd.conectarse();
            Persistencia.getInstancia().guardar(map, "Puesto");
            //bd.desconectar();
        } catch (SQLException e) {
            throw new AtencionException(999, "Error de SQL");
        } finally {
            bd.desconectar();
        }
    }

    public void actualizarDatos(Persistencia inst, BaseDatos bd, SistemaPersonas sistemaPersonas) throws AtencionException {
        MapeadorAtencion mapA = new MapeadorAtencion();
        String filtro = " idAtencion = " + this.getAtencionActual().getId();
        Atencion atencionBD = (Atencion) inst.buscar(mapA, filtro).get(0);
        this.atencionActual.setCliente(sistemaPersonas.buscarCliente(atencionBD.getCliente().getId()));
        this.atencionActual.setNumero(atencionBD.getNumero());
        this.atencionActual.setPuesto(this);
        this.atencionActual.setFchHraCreado(atencionBD.getFchHraCreado());
        this.atencionActual.setFchHraInicio(atencionBD.getFchHraInicio());
        this.atencionActual.setFchHraFin(atencionBD.getFchHraFin());
        this.atencionActual.setDescripcion(atencionBD.getDescripcion());
        this.atencionActual.setEstado(atencionBD.getEstado());
        if (atencionBD.getTrabInicio() != null) {
            Trabajador trab = sistemaPersonas.buscarTrabajador(atencionBD.getTrabInicio().getId());
            this.atencionActual.setTrabInicio(trab);
        }
        if (atencionBD.getTrabFin() != null) {
            Trabajador trab = sistemaPersonas.buscarTrabajador(atencionBD.getTrabFin().getId());
            this.atencionActual.setTrabFin(trab);
        }
        this.atencionActual.setCliente(sistemaPersonas.buscarCliente(atencionBD.getCliente().getId()));
    }
//</editor-fold>    
}
