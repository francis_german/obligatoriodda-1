package Modelo;

import Excepciones.AtencionException;
import Excepciones.ClienteException;
import Excepciones.LoginException;
import com.sun.jmx.mbeanserver.Util;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import persistencia.BaseDatos;
import persistencia.MapeadorCliente;
import persistencia.MapeadorTrabajador;
import persistencia.Persistencia;

public class SistemaPersonas extends Observable {

    private ArrayList<Cliente> clientes;
    private ArrayList<Trabajador> trabajadores;

    public SistemaPersonas() {
        this.clientes = new ArrayList<>();
        this.trabajadores = new ArrayList<>();
    }

    public void agregarTrabajador(Trabajador t) throws LoginException {
        if (this.trabajadores != null) {
            this.trabajadores.add(t);
            MapeadorTrabajador map = new MapeadorTrabajador(t);
            BaseDatos bd = BaseDatos.getInstancia();
            try{
                bd.conectarse();
                Persistencia.getInstancia().guardar(map, "Trabajador");
                //bd.desconectar();
            }
            catch(SQLException e){
                throw new LoginException(999, "Error de SQL");
            }
            finally{
                bd.desconectar();
            }
        }
    }

    public void agregarCliente(Cliente c) throws ClienteException {
        if (this.clientes != null) {
            this.clientes.add(c);
            MapeadorCliente map = new MapeadorCliente(c);
            BaseDatos bd = BaseDatos.getInstancia();
            try{
                bd.conectarse();
                Persistencia.getInstancia().guardar(map, "Cliente");
                //bd.desconectar();
            }
            catch(SQLException e){
                throw new ClienteException(999, "Error de SQL");
            }
            finally{
                bd.desconectar();
            }
        }
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public ArrayList<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public Trabajador validarLogin(String cedula, String pass) throws LoginException, AtencionException {

        Trabajador t = this.buscarTrabajador(cedula);
        //Si es null no se encontro
        if (t == null) {
            throw new LoginException(401, "El trabajador no existe");
        }
        if (!t.Validar(cedula, pass)){
            throw new LoginException(403, "Las credenciales ingresadas no son validas");
        }
        return t;
    }

//    public Cliente buscarCliente(int nroCliente) throws ClienteException {
//        MapeadorCliente map = new MapeadorCliente();
//        BaseDatos bd = BaseDatos.getInstancia();
//        try{
//            bd.conectarse();
//            this.clientes = (ArrayList<Cliente>)(List)Persistencia.getInstancia().todos(map);
//            for (Cliente unCliente : this.clientes) {
//                if (unCliente.getId() == nroCliente) {
//                    return unCliente;
//                }
//            }
//        }
//        catch(SQLException e){
//            throw new ClienteException(999, "Error de SQL");
//        }
//        finally{
//            bd.desconectar();
//        }
//        throw new ClienteException(101, "No existe un cliente con ese nro.");
//    }

    public Cliente buscarCliente(int idCli){
        for (Cliente cli : this.clientes){
            if (cli.getId() == idCli){
                return cli;
            }
        }
        
        return null;
    }
    
    public Trabajador buscarTrabajador(String documento) throws LoginException {
//        MapeadorTrabajador map = new MapeadorTrabajador();
//        BaseDatos bd = BaseDatos.getInstancia();
//        try{
//            bd.conectarse();
//            this.trabajadores = (ArrayList<Trabajador>)(List)Persistencia.getInstancia().todos(map);
//            //bd.desconectar();
//            for (Trabajador unTrabajador : this.trabajadores) {
//                if (unTrabajador.getDocumento().equals(documento)) {
//                    return unTrabajador;
//                }
//            }
//        }
//        catch (SQLException e){
//            throw new LoginException(999, "Error de SQL");
//        }
//        finally{
//            bd.desconectar();
//        }
        
        if (this.trabajadores != null && this.trabajadores.size() > 0){
            for (Trabajador trab : this.trabajadores){
                if (trab.getDocumento().equals(documento)) return trab;
            }
        }

        return null;
    }
    
    public Trabajador buscarTrabajador(int idTrab){
        if (this.trabajadores != null && this.trabajadores.size() > 0){
            for (Trabajador trab : this.trabajadores){
                if (trab.getId() == idTrab) return trab;
            }
        }
        return null;
    }
    
    public void actualizarDatos(Persistencia inst, BaseDatos bd){
        MapeadorTrabajador mapT = new MapeadorTrabajador();
        this.trabajadores = (ArrayList<Trabajador>)(List)inst.todos(mapT);
        MapeadorCliente mapC = new MapeadorCliente();
        this.clientes = (ArrayList<Cliente>)(List)inst.todos(mapC);
    }
}
