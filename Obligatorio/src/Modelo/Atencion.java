package Modelo;

import Excepciones.AtencionException;
import java.sql.SQLException;
import java.util.Date;
import persistencia.BaseDatos;
import persistencia.MapeadorAtencion;
import persistencia.Persistencia;

public class Atencion implements Comparable<Atencion> {

//<editor-fold defaultstate="collapsed" desc="** ATRIBUTOS **">
    private int id;
    private Date fchHraCreado;
    private int numero;
    private Cliente cliente;
    private Puesto puesto;
    private Date fchHraInicio;
    private Trabajador trabInicio;
    private Date fchHraFin;
    private Trabajador trabFin;
    private String descripcion;
    private EstadoAtencion estado;

//</editor-fold>
    
//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">   
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFchHraCreado() {
        return fchHraCreado;
    }

    public void setFchHraCreado(Date fchHraCreado) {
        this.fchHraCreado = fchHraCreado;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public Date getFchHraInicio() {
        return fchHraInicio;
    }

    public void setFchHraInicio(Date fchHraInicio) {
        this.fchHraInicio = fchHraInicio;
    }

    public Trabajador getTrabInicio() {
        return trabInicio;
    }

    public void setTrabInicio(Trabajador trabInicio) {
        this.trabInicio = trabInicio;
    }

    public Date getFchHraFin() {
        return fchHraFin;
    }

    public void setFchHraFin(Date fchHraFin) {
        this.fchHraFin = fchHraFin;
    }

    public Trabajador getTrabFin() {
        return trabFin;
    }

    public void setTrabFin(Trabajador trabFin) {
        this.trabFin = trabFin;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public EstadoAtencion getEstado() {
        return estado;
    }

    public void setEstado(EstadoAtencion estado) {
        this.estado = estado;
    }

    public double calcularCosto() throws AtencionException {
        if (this.cliente != null && this.cliente.getCosto() != null) {
            return this.cliente.getCosto().calcularCosto(this);
        }
        throw new AtencionException(555, "No se puede calcular el costo.");
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="** CONSTRUCTOR Y METODOS **">  
    public Atencion() {
    }

    public Atencion(Cliente cli, int nro) {
        this.fchHraCreado = new Date();
        this.numero = nro;
        this.cliente = cli;
    }

    public void inicioAtencion(Puesto puesto) throws AtencionException {
        this.puesto = puesto;
        this.trabInicio = puesto.getTrabajador();
        this.fchHraInicio = new Date();
        this.estado = EstadoAtencion.ABIERTA;
        this.guardarAtencion();

    }

    public void finalizoAtencion(Trabajador trabajador, String descr) throws AtencionException {
        this.trabFin = trabajador;
        this.fchHraFin = new Date();
        this.estado = EstadoAtencion.CERRADA;
        this.puesto.agregarTiempoAtencion(fchHraInicio, fchHraFin);
        this.descripcion = descr;
    }

    public void agregarDescripcion(String desc) throws AtencionException {
        this.descripcion = desc;
        this.guardarAtencion();
    }

    public void guardarAtencion() throws AtencionException {
        MapeadorAtencion map = new MapeadorAtencion(this);
        BaseDatos bd = BaseDatos.getInstancia();
        try {
            bd.conectarse();
            Persistencia.getInstancia().guardar(map, "Atencion");
            //bd.desconectar();
        } catch (SQLException e) {
            throw new AtencionException(999, "Error de SQL");
        } finally {
            bd.desconectar();
        }
    }

    public String EstadoToString() {

        if (this.estado == EstadoAtencion.ABIERTA) {
            return "Abierta";
        }
        if (this.estado == EstadoAtencion.CERRADA) {
            return "Cerrada";
        }
        if (this.estado == EstadoAtencion.EN_ESPERA) {
            return "En espera";
        }
        if (this.estado == EstadoAtencion.DERIVADA) {
            return "Derivada";
        }
        if (this.estado == EstadoAtencion.ASIGNADA) {
            return "Asignada";
        }
        return "";
    }

    //La cola de java nesecita sea comparable
    @Override
    public int compareTo(Atencion a) {
        return this.numero - a.numero;
    }

    public enum EstadoAtencion {
        EN_ESPERA, ABIERTA, CERRADA, DERIVADA, ASIGNADA
    }
//</editor-fold>
}
