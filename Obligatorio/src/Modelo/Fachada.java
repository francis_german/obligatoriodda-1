package Modelo;

import Costos.CostoFijo;
import Costos.Especial;
import Costos.Exonerado;
import Costos.Gestores;
import Costos.ICosto;
import Excepciones.AtencionException;
import Excepciones.ClienteException;
import Excepciones.LoginException;
import Excepciones.NumeradorException;
import Modelo.DTO.AtencionDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observer;
import persistencia.BaseDatos;
import persistencia.Persistencia;

public class Fachada {

    private static Fachada sistema;
    private final SistemaAtencion sistemaAtencion;
    private final SistemaPersonas sistemaPersonas;

//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">
    private Fachada() throws AtencionException {
        this.sistemaAtencion = new SistemaAtencion();
        this.sistemaPersonas = new SistemaPersonas();
        this.datosPrueba();

    }

    public static Fachada getSistema() throws AtencionException {
        if (sistema == null) {
            sistema = new Fachada();
        }
        return sistema;
    }

    public SistemaAtencion getSistemaAtencion() {
        return sistemaAtencion;
    }

    public SistemaPersonas getSistemaPersonas() {
        return sistemaPersonas;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public Area[] pidoAreas() throws AtencionException {
        this.actualizarDatos();
        return this.sistemaAtencion.pidoListaAreas();
    }

    //Pido los sectores menos el sector que se pasa por parametro
    public Sector[] pidoSectores(Area area, Sector s) throws AtencionException {
        this.actualizarDatos();
        return this.sistemaAtencion.pidoListaSectores(area, s);
    }

    public Atencion pidoNumero(int idSector, int nroCliente) throws NumeradorException, ClienteException, AtencionException {
        this.actualizarDatos();
        Cliente cliente = this.sistemaPersonas.buscarCliente(nroCliente);
        return this.sistemaAtencion.pidoNumero(idSector, cliente);
    }

    public Trabajador validarLogin(String cedula, String pass) throws LoginException, AtencionException {
        try {
            this.actualizarDatos();
            Trabajador trab = this.sistemaPersonas.validarLogin(cedula, pass);
            //trab.getSector().setArea(this.sistemaAtencion.BuscarArea(trab.getSector().getArea().getId()));
            if (this.estaTrabajando(trab)) {
                throw new LoginException(402, "El trabajador se encuentra trabajando.");
            } else {
                return trab;
            }
        } catch (AtencionException | LoginException e) {
            throw (e);
        }
    }

    public void asignarPuesto(Trabajador trabajador, Puesto puesto) throws AtencionException {
        this.actualizarDatos();
        this.getSistemaAtencion().asignarPuesto(trabajador, puesto);
    }

    public void iniciarAtencion(Puesto puesto) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.iniciarAtencion(puesto);
    }

    public void finalizoAtencion(Puesto puesto, String descr, boolean pedir) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.finalizoAtencion(puesto, descr, pedir);
    }

    private boolean estaTrabajando(Trabajador t) throws AtencionException {
        return this.sistemaAtencion.estaTrabajando(t);
    }

    public Sector[] pidoTodosSectores(Area area) throws AtencionException {
        this.actualizarDatos();
        return this.sistemaAtencion.pidoTodosSectores(area);
    }

    public Puesto[] pidoPuestosLibres(Trabajador trabajador) throws AtencionException {
        this.actualizarDatos();
        Sector sector = trabajador.getSector();
        return this.sistemaAtencion.pidoPuestosLibres(sector);
    }

    public ArrayList<Puesto> pidoPuestosParaDerivacion(Sector sector) throws AtencionException {
        this.actualizarDatos();
        return this.sistemaAtencion.pidoPuestosDerivacion(sector);
    }

    public void preguntarDerivacion(Puesto puesto, AtencionDTO dto) throws AtencionException {
        Atencion atencion = dto.getAtencion();
        atencion.guardarAtencion();
        this.actualizarDatos();
        this.sistemaAtencion.preguntarDerivacion(puesto, dto);
    }

    public void notificarDerivacion(Puesto puesto, AtencionDTO dto) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.notificarDerivacion(puesto, dto);
    }

    public void recibirDerivacion(Puesto puesto, AtencionDTO dto) throws AtencionException {
        Atencion atencion = dto.getAtencion();
        this.actualizarDatos();
        this.sistemaAtencion.recibirDerivacion(puesto, dto);
    }

    public void agregarObserverAlArea(Area area, Observer obs) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.agregarObserverAlArea(area, obs);
    }

    public void agregarObserverAlPuesto(Puesto puesto, Observer obs) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.agregarObserverAlPuesto(puesto, obs);
    }

    public void eliminarObserverDerivacion(Puesto puesto, Observer obs) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.eliminarObserverDerivacion(puesto, obs);
    }

    private void datosPrueba() throws AtencionException {
        try {
            Area areaComercial = new Area("Comercial");
            this.sistemaAtencion.agregarArea(areaComercial);

            Sector sRecepcion = new Sector("Recepcion", areaComercial);
            areaComercial.agregarSector(sRecepcion);
            Puesto p1 = new Puesto(sRecepcion.generarNuevoNroPuesto(), sRecepcion);
            Puesto p2 = new Puesto(sRecepcion.generarNuevoNroPuesto(), sRecepcion);
            Puesto p3 = new Puesto(sRecepcion.generarNuevoNroPuesto(), sRecepcion);
            sRecepcion.agregarPuesto(p1);
            sRecepcion.agregarPuesto(p2);
            sRecepcion.agregarPuesto(p3);

            Sector sAtencionAlCliente = new Sector("Atención de Clientes", areaComercial);
            areaComercial.agregarSector(sAtencionAlCliente);
            Puesto p4 = new Puesto(sAtencionAlCliente.generarNuevoNroPuesto(), sAtencionAlCliente);
            Puesto p5 = new Puesto(sAtencionAlCliente.generarNuevoNroPuesto(), sAtencionAlCliente);
            Puesto p6 = new Puesto(sAtencionAlCliente.generarNuevoNroPuesto(), sAtencionAlCliente);
            sAtencionAlCliente.agregarPuesto(p4);
            sAtencionAlCliente.agregarPuesto(p5);
            sAtencionAlCliente.agregarPuesto(p6);

            Sector sDespacho = new Sector("Despacho y Entrega", areaComercial);
            areaComercial.agregarSector(sDespacho);
            Puesto p7 = new Puesto(sDespacho.generarNuevoNroPuesto(), sDespacho);
            Puesto p8 = new Puesto(sDespacho.generarNuevoNroPuesto(), sDespacho);
            Puesto p9 = new Puesto(sDespacho.generarNuevoNroPuesto(), sDespacho);
            sDespacho.agregarPuesto(p7);
            sDespacho.agregarPuesto(p8);
            sDespacho.agregarPuesto(p9);

            Area areaFacturacion = new Area("Facturacion y Cobranzas");
            this.sistemaAtencion.agregarArea(areaFacturacion);

            Sector sMesaEntrada = new Sector("Mesa de Entrada", areaFacturacion);
            areaFacturacion.agregarSector(sMesaEntrada);
            Puesto p10 = new Puesto(sMesaEntrada.generarNuevoNroPuesto(), sMesaEntrada);
            Puesto p11 = new Puesto(sMesaEntrada.generarNuevoNroPuesto(), sMesaEntrada);
            Puesto p12 = new Puesto(sMesaEntrada.generarNuevoNroPuesto(), sMesaEntrada);
            sMesaEntrada.agregarPuesto(p10);
            sMesaEntrada.agregarPuesto(p11);
            sMesaEntrada.agregarPuesto(p12);

            Sector sEmisionFactura = new Sector("Emisión de Facturas", areaFacturacion);
            areaFacturacion.agregarSector(sEmisionFactura);
            Puesto p13 = new Puesto(sEmisionFactura.generarNuevoNroPuesto(), sEmisionFactura);
            Puesto p14 = new Puesto(sEmisionFactura.generarNuevoNroPuesto(), sEmisionFactura);
            Puesto p15 = new Puesto(sEmisionFactura.generarNuevoNroPuesto(), sEmisionFactura);
            sEmisionFactura.agregarPuesto(p13);
            sEmisionFactura.agregarPuesto(p14);
            sEmisionFactura.agregarPuesto(p15);

            Sector sCaja = new Sector("Caja", areaFacturacion);
            areaFacturacion.agregarSector(sCaja);
            Puesto p16 = new Puesto(sCaja.generarNuevoNroPuesto(), sCaja);
            Puesto p17 = new Puesto(sCaja.generarNuevoNroPuesto(), sCaja);
            Puesto p18 = new Puesto(sCaja.generarNuevoNroPuesto(), sCaja);
            sCaja.agregarPuesto(p16);
            sCaja.agregarPuesto(p17);
            sCaja.agregarPuesto(p18);

            Area areaReclamos = new Area("Reclamos");
            this.sistemaAtencion.agregarArea(areaReclamos);

            Sector sRecepcionReclamos = new Sector("Recepción de Reclamos", areaReclamos);
            areaReclamos.agregarSector(sRecepcionReclamos);
            Puesto p19 = new Puesto(sRecepcionReclamos.generarNuevoNroPuesto(), sRecepcionReclamos);
            Puesto p20 = new Puesto(sRecepcionReclamos.generarNuevoNroPuesto(), sRecepcionReclamos);
            Puesto p21 = new Puesto(sRecepcionReclamos.generarNuevoNroPuesto(), sRecepcionReclamos);
            sRecepcionReclamos.agregarPuesto(p19);
            sRecepcionReclamos.agregarPuesto(p20);
            sRecepcionReclamos.agregarPuesto(p21);

            Sector sAtencion = new Sector("Atención de Reclamos", areaReclamos);
            areaReclamos.agregarSector(sAtencion);
            Puesto p22 = new Puesto(sAtencion.generarNuevoNroPuesto(), sAtencion);
            Puesto p23 = new Puesto(sAtencion.generarNuevoNroPuesto(), sAtencion);
            Puesto p24 = new Puesto(sAtencion.generarNuevoNroPuesto(), sAtencion);
            sAtencion.agregarPuesto(p22);
            sAtencion.agregarPuesto(p23);
            sAtencion.agregarPuesto(p24);

            Sector sNotificacion = new Sector("Notificación de Reclamos", areaReclamos);
            areaReclamos.agregarSector(sNotificacion);
            Puesto p25 = new Puesto(sNotificacion.generarNuevoNroPuesto(), sNotificacion);
            Puesto p26 = new Puesto(sNotificacion.generarNuevoNroPuesto(), sNotificacion);
            Puesto p27 = new Puesto(sNotificacion.generarNuevoNroPuesto(), sNotificacion);
            sNotificacion.agregarPuesto(p25);
            sNotificacion.agregarPuesto(p26);
            sNotificacion.agregarPuesto(p27);

            //Datos de prueba trabajadores
            Trabajador t1 = new Trabajador("Francis", "1", "1", sRecepcion);
            Trabajador t2 = new Trabajador("Pedro", "2", "2", sRecepcion);
            Trabajador t3 = new Trabajador("Jose", "3", "3", sRecepcion);
            Trabajador t4 = new Trabajador("Luis", "4", "4", sMesaEntrada);
            Trabajador t5 = new Trabajador("Pepe", "5", "5", sMesaEntrada);
            Trabajador t6 = new Trabajador("Federico", "6", "6", sMesaEntrada);
            Trabajador t7 = new Trabajador("Francis", "7", "7", sAtencionAlCliente);
            Trabajador t8 = new Trabajador("Pedro", "8", "8", sAtencionAlCliente);
            Trabajador t9 = new Trabajador("Jose", "9", "9", sDespacho);
            Trabajador t10 = new Trabajador("Luis", "10", "10", sDespacho);
            Trabajador t11 = new Trabajador("Pepe", "11", "11", sEmisionFactura);
            Trabajador t12 = new Trabajador("Federico", "12", "12", sEmisionFactura);
            Trabajador t13 = new Trabajador("Pepe", "13", "13", sCaja);
            Trabajador t14 = new Trabajador("Federico", "14", "14", sCaja);
            Trabajador t15 = new Trabajador("Jose", "15", "15", sRecepcionReclamos);
            Trabajador t16 = new Trabajador("Luis", "16", "16", sRecepcionReclamos);
            Trabajador t17 = new Trabajador("Pepe", "17", "17", sAtencion);
            Trabajador t18 = new Trabajador("Federico", "18", "18", sAtencion);
            Trabajador t19 = new Trabajador("Pepe", "19", "19", sNotificacion);
            Trabajador t20 = new Trabajador("Federico", "20", "20", sNotificacion);

            this.sistemaPersonas.agregarTrabajador(t1);
            this.sistemaPersonas.agregarTrabajador(t2);
            this.sistemaPersonas.agregarTrabajador(t3);
            this.sistemaPersonas.agregarTrabajador(t4);
            this.sistemaPersonas.agregarTrabajador(t5);
            this.sistemaPersonas.agregarTrabajador(t6);
            this.sistemaPersonas.agregarTrabajador(t7);
            this.sistemaPersonas.agregarTrabajador(t8);
            this.sistemaPersonas.agregarTrabajador(t9);
            this.sistemaPersonas.agregarTrabajador(t10);
            this.sistemaPersonas.agregarTrabajador(t11);
            this.sistemaPersonas.agregarTrabajador(t12);
            this.sistemaPersonas.agregarTrabajador(t13);
            this.sistemaPersonas.agregarTrabajador(t14);
            this.sistemaPersonas.agregarTrabajador(t15);
            this.sistemaPersonas.agregarTrabajador(t16);
            this.sistemaPersonas.agregarTrabajador(t17);
            this.sistemaPersonas.agregarTrabajador(t18);
            this.sistemaPersonas.agregarTrabajador(t19);
            this.sistemaPersonas.agregarTrabajador(t20);

            Cliente c1 = new Cliente("maria@dda.com", "Maria", "1234564");
            Cliente c2 = new Cliente("paula@dda.com", "Paula", "1234563");
            Cliente c3 = new Cliente("claudia@dda.com", "Claudia", "1234562");
            Cliente c4 = new Cliente("ana@dda.com", "Ana", "1234561");
            Cliente c5 = new Cliente("laura@dda.com", "Laura", "1234597");
            Cliente c6 = new Cliente("silvia@dda.com", "Silvia", "1234557");
            ICosto costo = new CostoFijo();
            c1.setCosto(costo);
            c6.setCosto(costo);
            costo = new Especial();
            c2.setCosto(costo);
            costo = new Exonerado();
            c3.setCosto(costo);
            costo = new Gestores();
            c4.setCosto(costo);
            c5.setCosto(costo);
            
            this.sistemaPersonas.agregarCliente(c1);
            this.sistemaPersonas.agregarCliente(c2);
            this.sistemaPersonas.agregarCliente(c3);
            this.sistemaPersonas.agregarCliente(c4);
            this.sistemaPersonas.agregarCliente(c5);
            this.sistemaPersonas.agregarCliente(c6);
        } catch (LoginException | ClienteException e) {

        }
    }

    public void agregarDescripcion(Puesto puesto, String desc) throws AtencionException {
        this.actualizarDatos();
        this.sistemaAtencion.agregarDescripcion(puesto, desc);
    }

    public Area BuscarArea(int areaId) throws AtencionException {
        this.actualizarDatos();
        return this.sistemaAtencion.BuscarArea(areaId);
    }

    public Sector buscarSector(int sectorId) throws AtencionException {
        this.actualizarDatos();
        return this.sistemaAtencion.BuscarSector(sectorId);
    }

    private void actualizarDatos() throws AtencionException {
        Persistencia inst = Persistencia.getInstancia();
        BaseDatos bd = BaseDatos.getInstancia();
        try {
            bd.conectarse();
            this.sistemaPersonas.actualizarDatos(inst, bd);
            this.sistemaAtencion.actualizarDatos(inst, bd, this.sistemaPersonas);
        } catch (SQLException e) {

        } finally {
            bd.desconectar();
        }
    }

//</editor-fold>
}
