package Modelo;

import Excepciones.AtencionException;
import Excepciones.NumeradorException;
import Modelo.DTO.AtencionDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import persistencia.BaseDatos;
import persistencia.MapeadorArea;
import persistencia.Persistencia;

public class SistemaAtencion {

    private ArrayList<Area> areas;

//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">
    public ArrayList<Area> getAreas() {
        return areas;
    }

    public void setAreas(ArrayList<Area> areas) {
        this.areas = areas;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public SistemaAtencion() {
        this.areas = new ArrayList<>();
    }

    public void agregarArea(Area area) throws AtencionException {
        MapeadorArea map = new MapeadorArea(area);
        BaseDatos bd = BaseDatos.getInstancia();
        try {
            if (area != null) {
                bd.conectarse();
                Persistencia.getInstancia().guardar(map, "Area");
                //bd.desconectar();
            }
            this.cargarAreas();
        } catch (SQLException e) {
            throw new AtencionException(999, "Error de SQL");
        } finally {
            bd.desconectar();
        }
    }

    public Area[] pidoListaAreas() throws AtencionException {
//        this.cargarAreas();
        return this.areas.toArray(new Area[this.areas.size()]);
    }

    public Sector[] pidoListaSectores(Area area, Sector s) throws AtencionException {

        if (areaValida(area)) {
            area = this.BuscarArea(area.getId());
            //Primero agarro todos los sectores 
            ArrayList<Sector> lista = area.getSectores();
            //Ya los paso a array
            Sector[] sectores = lista.toArray(new Sector[lista.size()]);
            //Array de retorno que va a tener a todos los sectores menos, 
            //al que se pasa por parametro.
            Sector[] retorno = new Sector[sectores.length - 1];
            int cont = 0;
            for (Sector sec : sectores) {
                if (!sec.equals(s)) {
                    retorno[cont] = sec;
                    cont++;
                }
            }
            return retorno;
        }
        throw new AtencionException(313, "El area no existe");
    }

    public Atencion pidoNumero(int idSector, Cliente cliente) throws NumeradorException, AtencionException {
        Sector sector = this.BuscarSector(idSector);
//        this.cargarAreas();
        Area area = sector.getArea();
        if (areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Atencion a = area.pidoNumero(sector, cliente);
            return a;
        }
        throw new NumeradorException(201, "El área indicada no es valida");
    }

    public void asignarPuesto(Trabajador trabajador, Puesto puesto) throws AtencionException {
        Area area = puesto.getSector().getArea();

        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            area.asignarPuesto(trabajador, puesto);
        }
    }

    public void iniciarAtencion(Puesto puesto) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }

        puesto.iniciarAtencion();
    }

    public void finalizoAtencion(Puesto puesto, String descripcion, boolean pedir) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }
        puesto.finalizoAtencion(descripcion, pedir);
    }

    private boolean areaValida(Area area) throws AtencionException {
        //if (area == null) return false;
        area = this.BuscarArea(area.getId());
        return area != null;
    }

    public boolean estaTrabajando(Trabajador t) throws AtencionException {
        //this.cargarAreas();
        for (Area area : this.areas) {
            if (area.estaTrabajando(t)) {
                return true;
            }
        }
        return false;
    }

    public Sector[] pidoTodosSectores(Area area) throws AtencionException {
//        this.cargarAreas();
        ArrayList<Sector> sectores = area.getSectores();
        return sectores.toArray(new Sector[sectores.size()]);
    }

    public Puesto[] pidoPuestosLibres(Sector sector) throws AtencionException {
        Area area = sector.getArea();
        area = this.BuscarArea(area.getId());
        sector.setArea(area);
        if (area != null) {
            return area.pidoPuestosLibres(sector);
        }
        return null;
    }

    public ArrayList<Puesto> pidoPuestosDerivacion(Sector sector) throws AtencionException {
        return sector.puestosDerivacion();
    }

    public void preguntarDerivacion(Puesto puesto, AtencionDTO dto) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }
        puesto.preguntarDerivacion(dto);
    }

    public void notificarDerivacion(Puesto puesto, AtencionDTO dto) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }
        puesto.notificarDerivacion(dto);
    }

    public void recibirDerivacion(Puesto puesto, AtencionDTO dto) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }

        Puesto puestoOrigen = dto.getAtencion().getPuesto();
        Area areaOrigen = this.BuscarArea(puestoOrigen.getSector().getArea().getId());
        if (areaOrigen != null) {
            Sector sectorOrigen = areaOrigen.buscarSector(puestoOrigen.getSector().getId());
            if (sectorOrigen != null) {
                puestoOrigen = sectorOrigen.buscarPuesto(puestoOrigen.getId());
            }
        }
        puesto.recibirDerivacion(dto);
        puestoOrigen.quitarAtencion(true);
    }

    public void agregarObserverAlArea(Area area, Observer obs) {
        area.addObserver(obs);
    }

    public void agregarObserverAlPuesto(Puesto puesto, Observer obs) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }
        puesto.addObserver(obs);
    }

    public void eliminarObserverDerivacion(Puesto puesto, Observer obs) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }
        puesto.deleteObserver(obs);
    }

    public void agregarDescripcion(Puesto puesto, String desc) throws AtencionException {
        Area area = puesto.getSector().getArea();
        if (this.areaValida(area)) {
            area = this.BuscarArea(area.getId());
            Sector sector = puesto.getSector();
            if (area.sectorValido(sector)) {
                sector = area.buscarSector(sector.getId());
                puesto.setSector(sector);
            }
        }
        puesto.getAtencionActual().agregarDescripcion(desc);
    }

    public Area BuscarArea(int areaId) throws AtencionException {
        //this.cargarAreas();
        for (Area area : areas) {
            if (area.getId() == areaId) {
                return area;
            }
        }
        return null;
    }

    public Sector BuscarSector(int idSector) throws AtencionException {
//        this.cargarAreas();
        for (Area area : areas) {
            Sector s = area.buscarSector(idSector);
            if (s != null) {
                return s;
            }
        }
        return null;
    }

    private void cargarAreas() throws AtencionException {
        MapeadorArea map = new MapeadorArea();
        BaseDatos bd = BaseDatos.getInstancia();

        try {
            bd.conectarse();
            ArrayList<Area> aux = (ArrayList<Area>) (List) Persistencia.getInstancia().todos(map);
            //bd.desconectar();
            for (Area area : aux) {
                boolean esta = false;
                int i = 0;
                while (!esta && i < this.areas.size()) {
                    Area area2 = this.areas.get(i);
                    if (area.getNombre().equals(area2.getNombre())) {
                        esta = true;
                        area2.setId(area.getId());
                    }
                    i++;
                }
                if (!esta) {
                    this.areas.add(area);
                }
            }
        } catch (SQLException e) {
            throw new AtencionException(999, "Error de SQL");
        } finally {
            bd.desconectar();
        }
    }

    void actualizarDatos(Persistencia inst, BaseDatos bd, SistemaPersonas sistemaPersonas) throws AtencionException {
        MapeadorArea mapA = new MapeadorArea();
        ArrayList<Area> areasBD = (ArrayList<Area>) (List) inst.todos(mapA);
        for (Area area : areasBD) {
            boolean esta = false;
            int i = 0;
            while (!esta && i < this.areas.size()) {
                if (area.getId() == this.areas.get(i).getId()) {
                    esta = true;
                    this.areas.get(i).setNombre(area.getNombre());
                    this.areas.get(i).actualizarDatos(inst, bd, sistemaPersonas);
                }
                i++;
            }
            if (!esta) {
                this.areas.add(area);
                this.areas.get(i).actualizarDatos(inst, bd, sistemaPersonas);
            }
        }
    }
//</editor-fold>    
}
