package Modelo;

import Excepciones.AtencionException;
import Excepciones.NumeradorException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import persistencia.BaseDatos;
import persistencia.MapeadorSector;
import persistencia.Persistencia;

public class Area extends Observable {

    private int id;
    private static int ultId;
    private String nombre;
    private ArrayList<Sector> sectores;

//<editor-fold defaultstate="collapsed" desc="** PROPIEDADES **">
    public int getId() {
        return this.id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Sector> getSectores() throws AtencionException {
//       this.cargarSectores();
        return sectores;
    }
    //</editor-fold>

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public Area() {
        //this.id = ++ultId;
        this.sectores = new ArrayList<>();
    }

    public Area(String nombre) {
        this.nombre = nombre;
        this.sectores = new ArrayList<>();
//        this.id = ++ultId;
    }

    public void agregarSector(Sector sector) throws AtencionException {
        if (sector != null) {
            this.sectores.add(sector);
            sector.guardarSector();
        } else {
            throw new AtencionException(301, "El dato recibido para agregar el sector es nulo");
        }
    }

    public Atencion pidoNumero(Sector sector, Cliente cli) throws NumeradorException, AtencionException {
        if (this.sectorValido(sector)) {
            sector = this.buscarSector(sector.getId());
            Atencion a = sector.doyNumero(cli);
            return a;
        }

        throw new NumeradorException(201, "No se encontro el sector indicado");
    }

    public void asignarPuesto(Trabajador trabajador, Puesto puesto) throws AtencionException {
        Sector sector = puesto.getSector();
        if(this.sectorValido(sector)){
            sector = this.buscarSector(sector.getId());
            sector.asignarPuesto(trabajador, puesto);
        }
    }

    public boolean sectorValido(Sector sector) throws AtencionException {
        sector = this.buscarSector(sector.getId());
        return sector != null;
    }

    public void avisarAtencionAsignado(Atencion atencion) {
        this.setChanged();
        this.notifyObservers(atencion);
    }

    public boolean estaTrabajando(Trabajador t) throws AtencionException {
        //this.cargarSectores();
        for (Sector sec : this.sectores){
            if (sec.estaTrabajando(t)) return true;
        }
        return false;
    }

    public Puesto[] pidoPuestosLibres(Sector sector) throws AtencionException {
        sector = this.buscarSector(sector.getId());
        return sector.puestosLibres();
    }

    @Override
    public String toString() {
        return this.nombre;
    }

    public Sector buscarSector(int idSector) throws AtencionException {
//        MapeadorSector map = new MapeadorSector();
//        String filtro = "idArea = " + this.id + " and idSector = " + idSector;
//        BaseDatos bd = BaseDatos.getInstancia();
//
//        try{
//            bd.conectarse();
//            ArrayList<Sector> aux = (ArrayList<Sector>)(List)Persistencia.getInstancia().buscar(map, filtro);
//            //bd.desconectar();
//            if (aux != null && aux.size() > 0){
//                Sector sector = aux.get(0);
//                sector.setArea(this);
//                for(Sector sec : this.sectores){
//                    if (sec.getId() == sector.getId()){
//                        sector.setListaEspera(sec.getListaEspera());
//                        sector.setPuestos(sec.getPuestos());
//                        sec = sector;
//                    }
//                }
//                return sector;
//            }
//        }
//        catch(SQLException e){
//            throw new AtencionException(999, "Error de SQL");
//        }
//        finally{
//            bd.desconectar();
//        }
        
//        this.cargarSectores();
        for (Sector s : sectores) {
            if (s.getId() == idSector) {
                return s;
            }
        }
        return null;
    }

    public Sector[] getSectoresArray() throws AtencionException {
        //this.cargarSectores();
        int cant = this.sectores.size();
        return this.sectores.toArray(new Sector[cant]);
    }

//    public void cargarSectores() throws AtencionException {
//        MapeadorSector map = new MapeadorSector();
//        String filtro = "idArea = " + this.id;
//        BaseDatos bd = BaseDatos.getInstancia();
//        
//        try{
//            bd.conectarse();
//            ArrayList<Sector> aux = (ArrayList<Sector>)(List)Persistencia.getInstancia().buscar(map, filtro);
//            //bd.desconectar();
//            for(Sector sec : aux){
//                sec.setArea(this);
//                boolean esta = false;
//                int i = 0;
//                while (!esta && i < this.sectores.size()){
//                    Sector sec2 = this.sectores.get(i);
//                    if(sec.getId() == sec2.getId()){
//                        esta = true;
//                        sec2.setArea(sec.getArea());
//                        sec2.setNombre(sec.getNombre());
//                        sec2.setUltNroAtencion(sec.getUltNroAtencion());
//                        sec2.setUltNumeroPuesto(sec.getUltNumeroPuesto());
//                    }
//                    i++;
//                }
//                if (!esta){
//                    this.sectores.add(sec);
//                }
//            }
//        }
//        catch(SQLException e){
//            throw new AtencionException(999, "Error de SQL");
//        }
//        finally{
//            bd.desconectar();
//        }
//    }
    
    public void actualizarDatos(Persistencia inst, BaseDatos bd, SistemaPersonas sistemaPersonas) throws AtencionException{
        MapeadorSector mapS = new MapeadorSector();
        String filtro = " idArea = " + this.getId();
        ArrayList<Sector> sectorBD = (ArrayList<Sector>)(List)inst.buscar(mapS, filtro);
        for(Sector sector : sectorBD){
            boolean esta = false;
            int i = 0;
            while (!esta && i < this.sectores.size()){
                if(sector.getId() == this.sectores.get(i).getId()){
                    esta = true;
                    this.sectores.get(i).setArea(this);
                    this.sectores.get(i).setNombre(sector.getNombre());
                    this.sectores.get(i).setUltNroAtencion(sector.getUltNroAtencion());
                    this.sectores.get(i).setUltNumeroPuesto(sector.getUltNumeroPuesto());
                    this.sectores.get(i).actualizarDatos(inst, bd, sistemaPersonas);
                }
                i++;
            }
            if (!esta){
                this.sectores.add(sector);
                this.sectores.get(i).actualizarDatos(inst, bd, sistemaPersonas);
            }
        }
        
        for (Trabajador trab : sistemaPersonas.getTrabajadores()){
            Sector sec = this.buscarSector(trab.getSector().getId());
            if (sec != null) trab.setSector(sec);
        }
    }
//</editor-fold>    
}
