package Modelo.DTO;

import Modelo.Atencion;

public class AtencionDTO {

    private Atencion atencion;
    private boolean aceptada;
    private boolean pregunta;
    private boolean ultimoPuesto;
    private Tipo tipo;

    public enum Tipo {
        ATENCION, DERIVACION
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public boolean isUltimoPuesto() {
        return ultimoPuesto;
    }

    public void setUltimoPuesto(boolean ultimoPuesto) {
        this.ultimoPuesto = ultimoPuesto;
    }

    public boolean isPregunta() {
        return pregunta;
    }

    public boolean isRespuesta() {
        return !pregunta;
    }

    public void setRespuesta(boolean res) {
        this.pregunta = !res;
    }

    public void setPregunta(boolean pregunta) {
        this.pregunta = pregunta;
    }

    public Atencion getAtencion() {
        return atencion;
    }

    public void setAtencion(Atencion atencion) {
        this.atencion = atencion;
    }

    public boolean isAceptada() {
        return aceptada;
    }

    public void setAceptada(boolean aceptada) {
        this.aceptada = aceptada;
    }

}
