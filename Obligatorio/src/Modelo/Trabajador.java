package Modelo;

public class Trabajador {

    private int id;
    private static int ultId;
    private String nombreCompleto;
    private String documento;
    private String pass;
    private Sector sector;

    public Trabajador(){}
    
    public Trabajador(String nombre, String doc, String pass, Sector s) {
        //this.id = ++ultId;
        this.nombreCompleto = nombre;
        this.documento = doc;
        this.pass = pass;
        this.sector = s;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static int getUltId() {
        return ultId;
    }

    public static void setUltId(int ultId) {
        Trabajador.ultId = ultId;
    }

    public boolean Validar(String cedula, String pass) {
        return this.documento.equals(cedula.trim()) && this.pass.equals(pass.trim());
    }

}
