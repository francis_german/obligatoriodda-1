package Modelo.Estructuras;

public class Cola<T> {

    private NodoCola<T> inicio;
    private NodoCola<T> fin;
    private int cantElementos;

    public void encolar(T dato) {
        if (cantElementos == 0) {
            inicio = new NodoCola<T>(dato);
            fin = new NodoCola<T>(dato);
        } else {
            fin.setSig(new NodoCola<T>(dato));
            fin = fin.getSig();
        }
        cantElementos++;
    }

    public T desencolar() {
        T sale = inicio.getDato();
        inicio = inicio.getSig();
        cantElementos--;
        if (cantElementos == 0) {
            fin = null;
        }
        return sale;
    }

    public boolean esVacia() {
        return cantElementos == 0;
    }

    public int lenght() {
        return this.cantElementos;
    }
}
