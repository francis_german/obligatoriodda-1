package Utilidades;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class Utilidades {

    public static String FechaToString(Date d) {
        DateFormat df = new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy '-' HH:mm:ss");
        return df.format(d).toUpperCase();
    }

    public static void mostrarError(String msg, JDialog dialog) {
        JOptionPane.showMessageDialog(dialog.getOwner(), msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void mostrarMsg(String msg, JDialog dialog) {
        JOptionPane.showMessageDialog(dialog.getOwner(), msg, "Info", JOptionPane.INFORMATION_MESSAGE);
    }

}
