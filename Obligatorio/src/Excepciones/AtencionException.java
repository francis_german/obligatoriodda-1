package Excepciones;

public class AtencionException extends AppException {

    public AtencionException(int codigo, String mensaje) {
        super(codigo, mensaje);
    }
}
