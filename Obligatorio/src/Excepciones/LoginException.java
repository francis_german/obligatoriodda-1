package Excepciones;

public class LoginException extends AppException {

    public LoginException(int codigo, String mensaje) {
        super(codigo, mensaje);
    }
}
