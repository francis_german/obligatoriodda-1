
package Excepciones;

public class DerivacionException extends AppException {
    
    public DerivacionException(int codigo, String mensaje) {
        super(codigo, mensaje);
    }
}
