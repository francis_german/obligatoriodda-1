package Excepciones;

public class ClienteException extends AppException {

    public ClienteException(int codigo, String mensaje) {
        super(codigo, mensaje);
    }
}
