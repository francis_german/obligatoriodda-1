
package Excepciones;

public class MonitorException extends AppException {
    
    public MonitorException(int codigo, String mensaje) {
        super(codigo, mensaje);
    } 
}
