package Excepciones;

public class NumeradorException extends AppException {

    public NumeradorException(int codigo, String mensaje) {
        super(codigo, mensaje);
    }
}
