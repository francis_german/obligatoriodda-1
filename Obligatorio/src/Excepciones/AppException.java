package Excepciones;

public abstract class AppException extends Exception {

    private final int codigo;

    public int getCodigo() {
        return codigo;
    }

    public AppException(int codigo, String msg) {
        super(msg);
        this.codigo = codigo;
    }

}
