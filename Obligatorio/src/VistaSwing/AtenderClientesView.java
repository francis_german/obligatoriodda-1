package VistaSwing;

import Controlador.AtenderClientesController;
import Controlador.Interfaces.IAtenderView;
import Excepciones.AtencionException;
import Modelo.Atencion;
import Modelo.Puesto;
import Utilidades.Utilidades;
import java.awt.Frame;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

public class AtenderClientesView extends javax.swing.JDialog implements IAtenderView {

    private final AtenderClientesController controlador;

    public AtenderClientesView(java.awt.Frame parent, boolean modal, Puesto p) throws AtencionException {
        super(parent, modal);
        initComponents();
        this.txtDescr.setEditable(false);
        this.controlador = new AtenderClientesController(this, p);
        pack();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlAtencion = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblFchCreado = new javax.swing.JLabel();
        lblEstado = new javax.swing.JLabel();
        lblFchInicio = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        pnlEncabezado = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblSubMensaje = new javax.swing.JLabel();
        lblNroAtencion = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescr = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        btnAbrirAtencion = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        lblAreaSector = new javax.swing.JLabel();
        lblEstadisticas = new javax.swing.JLabel();
        btnFinAtencion = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblCantidadAt = new javax.swing.JLabel();
        lblCantDerivados = new javax.swing.JLabel();
        btnDerivar = new javax.swing.JButton();
        btnTerminarSalir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Atender Clientes");
        setLocationByPlatform(true);
        setUndecorated(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        pnlAtencion.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel5.setText("Estado:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel7.setText("Hora  Inicio:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel8.setText("Hora Creada:");

        lblFchCreado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblFchCreado.setText("HORA CREADO ATENCION");

        lblEstado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblEstado.setText("ESTADO ATENCION");

        lblFchInicio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblFchInicio.setText("HORA INICIO ATENCION");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 204, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Informacion de Atencion:");

        javax.swing.GroupLayout pnlAtencionLayout = new javax.swing.GroupLayout(pnlAtencion);
        pnlAtencion.setLayout(pnlAtencionLayout);
        pnlAtencionLayout.setHorizontalGroup(
            pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAtencionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlAtencionLayout.createSequentialGroup()
                        .addGroup(pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblEstado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblFchInicio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblFchCreado, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlAtencionLayout.setVerticalGroup(
            pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAtencionLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(32, 32, 32)
                .addGroup(pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(lblFchCreado))
                .addGap(18, 18, 18)
                .addGroup(pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblFchInicio))
                .addGap(18, 18, 18)
                .addGroup(pnlAtencionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lblEstado))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlEncabezado.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        lblTitulo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setText("MENSAJE");

        lblSubMensaje.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblSubMensaje.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSubMensaje.setText("MENSAJE SI HAY ATENCIONES");

        lblNroAtencion.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lblNroAtencion.setForeground(new java.awt.Color(0, 204, 0));
        lblNroAtencion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNroAtencion.setText("0");
        lblNroAtencion.setBorder(javax.swing.BorderFactory.createTitledBorder("Numero Actual"));

        javax.swing.GroupLayout pnlEncabezadoLayout = new javax.swing.GroupLayout(pnlEncabezado);
        pnlEncabezado.setLayout(pnlEncabezadoLayout);
        pnlEncabezadoLayout.setHorizontalGroup(
            pnlEncabezadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEncabezadoLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(lblNroAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlEncabezadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSubMensaje, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlEncabezadoLayout.setVerticalGroup(
            pnlEncabezadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEncabezadoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlEncabezadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblNroAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pnlEncabezadoLayout.createSequentialGroup()
                        .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblSubMensaje)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("Descripcion:");

        txtDescr.setColumns(20);
        txtDescr.setRows(5);
        jScrollPane1.setViewportView(txtDescr);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        jPanel3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnAbrirAtencion.setText("Iniciar");
        btnAbrirAtencion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAbrirAtencionActionPerformed(evt);
            }
        });

        btnLogout.setBackground(new java.awt.Color(255, 51, 51));
        btnLogout.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnLogout.setForeground(new java.awt.Color(255, 255, 255));
        btnLogout.setText("LOGOUT");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        lblAreaSector.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblAreaSector.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAreaSector.setText("MENSAJE AREA Y SECTOR QUE ESTA EL PUESTO");

        lblEstadisticas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblEstadisticas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblEstadisticas.setText("ESTADISTICAS DEL PUESTO");

        btnFinAtencion.setText("Terminar");
        btnFinAtencion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinAtencionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAbrirAtencion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFinAtencion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAreaSector, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
                    .addComponent(lblEstadisticas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(lblAreaSector, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblEstadisticas, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(36, 36, 36))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnLogout, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAbrirAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnFinAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        jPanel2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        lblCantidadAt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lblCantidadAt.setForeground(new java.awt.Color(0, 204, 0));
        lblCantidadAt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCantidadAt.setText("0");
        lblCantidadAt.setBorder(javax.swing.BorderFactory.createTitledBorder("Cantidad Atenciones"));

        lblCantDerivados.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lblCantDerivados.setForeground(new java.awt.Color(0, 204, 0));
        lblCantDerivados.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCantDerivados.setText("0");
        lblCantDerivados.setBorder(javax.swing.BorderFactory.createTitledBorder("Derivados Espera"));

        btnDerivar.setText("Derivar Atencion");
        btnDerivar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDerivarActionPerformed(evt);
            }
        });

        btnTerminarSalir.setBackground(new java.awt.Color(255, 102, 102));
        btnTerminarSalir.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnTerminarSalir.setForeground(new java.awt.Color(255, 255, 255));
        btnTerminarSalir.setText("Terminar y Salir");
        btnTerminarSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTerminarSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCantidadAt, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblCantDerivados, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnTerminarSalir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnDerivar)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnTerminarSalir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDerivar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCantidadAt)
                        .addComponent(lblCantDerivados, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)))
                .addGap(14, 14, 14))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlEncabezado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlAtencion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(pnlEncabezado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pnlAtencion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        AppTrabajadores ventana = (AppTrabajadores) this.getOwner();
        ventana.dispose();
    }//GEN-LAST:event_formWindowClosed

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        try {
            this.controlador.logout();
            this.dispose();
        } catch (Exception ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }
    }//GEN-LAST:event_btnLogoutActionPerformed

    int xx = 0;
    int yy = 0;

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        xx = evt.getX();
        yy = evt.getY();
    }//GEN-LAST:event_formMousePressed

    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xx, y - yy);
    }//GEN-LAST:event_formMouseDragged

    private void btnAbrirAtencionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAbrirAtencionActionPerformed
        //Abrir atencion
        try {
            this.controlador.abrirAtencion();
            this.txtDescr.setEditable(true);
        } catch (Exception ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }


    }//GEN-LAST:event_btnAbrirAtencionActionPerformed

    private void btnFinAtencionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinAtencionActionPerformed
        try {
            String descripcion = txtDescr.getText().trim();
            this.controlador.finalizarAtencion(descripcion, true);
            this.limpiarDescr();
            this.txtDescr.setEditable(false);
        } catch (Exception ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }
    }//GEN-LAST:event_btnFinAtencionActionPerformed

    private void btnDerivarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDerivarActionPerformed
        try {
            this.controlador.derivacion();
        } catch (AtencionException ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }
    }//GEN-LAST:event_btnDerivarActionPerformed

    private void btnTerminarSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTerminarSalirActionPerformed
        try {
            String descripcion = txtDescr.getText().trim();
            this.controlador.finalizarAtencion(descripcion, false);
            this.limpiarDescr();
            this.txtDescr.setEditable(false);
            this.controlador.logout();
            this.dispose();
        } catch (Exception ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }
    }//GEN-LAST:event_btnTerminarSalirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAbrirAtencion;
    private javax.swing.JButton btnDerivar;
    private javax.swing.JButton btnFinAtencion;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnTerminarSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAreaSector;
    private javax.swing.JLabel lblCantDerivados;
    private javax.swing.JLabel lblCantidadAt;
    private javax.swing.JLabel lblEstadisticas;
    private javax.swing.JLabel lblEstado;
    private javax.swing.JLabel lblFchCreado;
    private javax.swing.JLabel lblFchInicio;
    private javax.swing.JLabel lblNroAtencion;
    private javax.swing.JLabel lblSubMensaje;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JPanel pnlAtencion;
    private javax.swing.JPanel pnlEncabezado;
    private javax.swing.JTextArea txtDescr;
    // End of variables declaration//GEN-END:variables

    @Override
    public void mostrarAtencion(Atencion a) {
        if (a != null) {
            String subMensaje = "Atendiendo a " + a.getCliente().getNombreCompleto();
            String nroAtencion = a.getNumero() + "";
            String horaCreada = Utilidades.FechaToString(a.getFchHraCreado());
            String horaInicio = a.getEstado() == Atencion.EstadoAtencion.ABIERTA ? Utilidades.FechaToString(a.getFchHraInicio()) : "No tiene";
            String estado = a.EstadoToString();

            this.lblNroAtencion.setText(nroAtencion);
            this.lblFchCreado.setText(horaCreada);
            this.lblFchInicio.setText(horaInicio);
            this.lblEstado.setText(estado);
            this.lblSubMensaje.setText(subMensaje);
            this.txtDescr.setText(a.getDescripcion());
        }
    }

    private void limpiarDescr() {
        this.txtDescr.setText("");
    }

    @Override
    public void actualizarEstadisticas(Puesto p) {
        String estadisticas = "Tiempo Promedio: " + p.TiempoPromedioStr();
        String cantAtenciones = p.getNumerosAsignados() + "";
        String cantDerivados = p.cantNroDerivados() + "";

        this.lblCantidadAt.setText(cantAtenciones);
        this.lblCantDerivados.setText(cantDerivados);
        this.lblEstadisticas.setText(estadisticas);
    }

    @Override
    public void mostrarPuestoVacio() {

        String subMensaje = "No tiene ninguna atencion asginada";
        String nroAtencion = "S/N";
        String horaCreada = "Sin hora";
        String horaInicio = "Sin hora";
        String estado = "Sin estado";

        this.lblNroAtencion.setText(nroAtencion);
        this.lblFchCreado.setText(horaCreada);
        this.lblFchInicio.setText(horaInicio);
        this.lblEstado.setText(estado);
        this.lblSubMensaje.setText(subMensaje);
        this.limpiarDescr();
    }

    @Override
    public void nuevaDerivacion(int cant) {
        this.lblCantDerivados.setText(cant + "");
    }

    @Override
    public void cargarInfo(String msgTitulo, String msgArea) {
        this.lblTitulo.setText(msgTitulo);
        this.lblAreaSector.setText(msgArea);
    }

    private void controlesEnabled(boolean e) {
        this.btnAbrirAtencion.setEnabled(e);
        this.btnDerivar.setEnabled(e);
        this.btnFinAtencion.setEnabled(e);
        this.btnLogout.setEnabled(e);
    }

    @Override
    public void activarControles() {
        this.controlesEnabled(true);
    }

    @Override
    public void desactivarControles() {
        this.controlesEnabled(false);
    }

    @Override
    public void mensajeError(String msg) {
        Utilidades.mostrarError(msg, this);
    }

    @Override
    public void actualizarCantDerivados(int cant) {
        this.lblCantDerivados.setText(cant + "");
    }

    @Override
    public void abrirDerivacion(Puesto p) {
        //Derivacion de atenciones.
        try {
            String descr = txtDescr.getText().trim();
            DerivarNumeroView vistaDerivar = new DerivarNumeroView((Frame) this.getOwner(), false, p, descr, this);
            vistaDerivar.setVisible(true);
            this.desactivarControles();

        } catch (AtencionException ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }
    }

    private JDialog d;

    @Override
    public int preguntarDerivacion(String msg) {
        d = new JDialog(this.getOwner());
        return JOptionPane.showConfirmDialog(d, msg, "Confirmar", JOptionPane.YES_NO_OPTION);
    }

    @Override
    public void cerrarPregunta() {
        d.dispose();
    }
}
