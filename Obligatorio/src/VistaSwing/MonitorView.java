package VistaSwing;

import Controlador.Interfaces.IMonitorView;
import Controlador.MonitorViewController;
import Excepciones.AtencionException;
import Modelo.Area;
import Modelo.Atencion;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class MonitorView extends javax.swing.JDialog implements IMonitorView {

    private final MonitorViewController controlador;
    private final int cantLineas;

    public MonitorView(java.awt.Frame parent, boolean modal, Area area, int cant) throws AtencionException {
        super(parent, modal);
        this.cantLineas = cant;
        initComponents();
        DefaultTableModel modelo = (DefaultTableModel) this.tblMonitor.getModel();
        modelo.setNumRows(cant);
        TableColumnModel colMod = (TableColumnModel) this.tblMonitor.getColumnModel();
        colMod.getColumn(0).setMinWidth(90);
        colMod.getColumn(0).setMinWidth(60);
        colMod.getColumn(1).setMaxWidth(60);
        colMod.getColumn(2).setMinWidth(90);
        colMod.getColumn(3).setMinWidth(60);
        colMod.getColumn(3).setMaxWidth(60);
        this.controlador = new MonitorViewController(this, area.getId());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblMonitor = new javax.swing.JTable();
        txtTitulo = new javax.swing.JTextField();
        lblUltimaAt = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tblMonitor.setBackground(java.awt.SystemColor.control);
        tblMonitor.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblMonitor.setForeground(new java.awt.Color(51, 51, 255));
        tblMonitor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Sector", "Nro.", "Cliente", "Puesto"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblMonitor.setFillsViewportHeight(true);
        tblMonitor.setRowHeight(30);
        tblMonitor.setRowSelectionAllowed(false);
        jScrollPane1.setViewportView(tblMonitor);

        txtTitulo.setEditable(false);
        txtTitulo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTitulo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTitulo.setText("NOMBRE DEL AREA");

        lblUltimaAt.setBackground(new java.awt.Color(255, 255, 255));
        lblUltimaAt.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblUltimaAt.setForeground(new java.awt.Color(51, 153, 0));
        lblUltimaAt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblUltimaAt.setText("S/N");
        lblUltimaAt.setBorder(javax.swing.BorderFactory.createTitledBorder("Ultimo Asignado"));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(lblUltimaAt, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addComponent(txtTitulo)
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblUltimaAt, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
                    .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblUltimaAt;
    private javax.swing.JTable tblMonitor;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables

    @Override
    public void actualizoMonitor(Atencion atencion, String nro) {

        if (atencion != null) {
            DefaultTableModel modelo = (DefaultTableModel) this.tblMonitor.getModel();
            if (modelo.getRowCount() > this.cantLineas - 1) {
                modelo.removeRow(this.cantLineas - 1);
            }
            String sector = "";
            int puesto = 0;
            if (atencion.getPuesto() != null) {
                sector = atencion.getPuesto().getSector().getNombre();
                puesto = atencion.getPuesto().getNroPuesto();
            }

            this.lblUltimaAt.setText(nro);

            Object[] fila = new Object[]{
                sector,
                nro,
                atencion.getCliente().getNombreCompleto(),
                puesto
            };
            modelo.insertRow(0, fila);
        }
    }

    @Override
    public void mostrarTitulo(String msg) {
        this.txtTitulo.setText("Monitor del Area: " + msg);
    }

}
