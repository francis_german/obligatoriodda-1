package VistaSwing;

import Controlador.AplicacionController;
import Controlador.Interfaces.IVista;
import Excepciones.AtencionException;
import Utilidades.Utilidades;
import java.awt.event.ActionEvent;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Aplicacion extends javax.swing.JFrame implements IVista {

    private final AplicacionController controlador;

    public Aplicacion() throws AtencionException {
        initComponents();
        this.controlador = new AplicacionController(this);
        this.reloj();
        this.setVisible(true);
    }

    private void reloj() {
        Timer timer = new Timer(1000, (ActionEvent e) -> {
            actualizarHora();
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.start();
    }

    private void actualizarHora() {
        Date fchActual = new Date();
        String fchActualStr = Utilidades.FechaToString(fchActual);
        this.lblFchHora.setText(fchActualStr);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnClientes = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnTrabajadores = new javax.swing.JButton();
        btnMonitoreo = new javax.swing.JButton();
        lblFchHora = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Obligatorio Diseño y Desarrollo de Aplicaciones");

        btnClientes.setText("Aplicacion Clientes");
        btnClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClientesActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        jLabel1.setText("Obligatorio");

        jLabel2.setText("Francis Espindola N° 195.559");

        jLabel3.setText("Jose Luis Soanes N° 177.214 ");

        btnTrabajadores.setText("Aplicacion Trabajadores");
        btnTrabajadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrabajadoresActionPerformed(evt);
            }
        });

        btnMonitoreo.setText("Aplicacion Monitoreo");
        btnMonitoreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMonitoreoActionPerformed(evt);
            }
        });

        lblFchHora.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblFchHora.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnTrabajadores)
                        .addGap(18, 18, 18)
                        .addComponent(btnClientes)
                        .addGap(18, 18, 18)
                        .addComponent(btnMonitoreo)
                        .addContainerGap(70, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(81, 81, 81))))
            .addComponent(lblFchHora, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(77, 77, 77)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMonitoreo, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTrabajadores, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(lblFchHora, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClientesActionPerformed
        try
        {
        AppClientes vista = new AppClientes();
        vista.setVisible(true);
        }
        catch (AtencionException e)
        {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnClientesActionPerformed

    private void btnTrabajadoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrabajadoresActionPerformed
        AppTrabajadores vista = new AppTrabajadores();
        vista.setVisible(true);
    }//GEN-LAST:event_btnTrabajadoresActionPerformed

    private void btnMonitoreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMonitoreoActionPerformed
        try
        {
            AppMonitoreo vista = new AppMonitoreo();
            vista.setVisible(true);
        }
        catch (AtencionException e)
        {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnMonitoreoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClientes;
    private javax.swing.JButton btnMonitoreo;
    private javax.swing.JButton btnTrabajadores;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblFchHora;
    // End of variables declaration//GEN-END:variables
}
