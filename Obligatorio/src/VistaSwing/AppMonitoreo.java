package VistaSwing;

import Controlador.AppMonitoreoController;
import Controlador.Interfaces.IAppMonitoreo;
import Excepciones.AtencionException;
import Modelo.Area;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class AppMonitoreo extends javax.swing.JFrame implements IAppMonitoreo{

    private final AppMonitoreoController controlador;

    public AppMonitoreo() throws AtencionException {
        this.controlador = new AppMonitoreoController(this);
        initComponents();
        this.controlador.cargarListaAreas();
    }
    
    @Override
    public void cargoAreas(Area[] areas) {
        DefaultComboBoxModel<Area> m = new DefaultComboBoxModel<>();
        for (Area area : areas) {
            m.addElement(area);
        }
        lstArea.setModel(m);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lstArea = new javax.swing.JComboBox<>();
        lblArea = new javax.swing.JLabel();
        btnIniciar = new javax.swing.JButton();
        lblTitulo = new javax.swing.JLabel();
        lblCantLineas = new javax.swing.JLabel();
        txtCantLineas = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Aplicacion Monitoreo");

        lstArea.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        lblArea.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblArea.setText("Seleccione el Area");

        btnIniciar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnIniciar.setText("Iniciar Monitor");
        btnIniciar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });

        lblTitulo.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        lblTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTitulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Fotos/monitor.jpg"))); // NOI18N

        lblCantLineas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblCantLineas.setText("Cantidad de Lineas");

        txtCantLineas.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtCantLineas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCantLineas.setText("10");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCantLineas)
                            .addComponent(lblArea))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lstArea, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtCantLineas)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnIniciar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTitulo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 485, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblArea)
                        .addGap(30, 30, 30))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lstArea, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblCantLineas)
                    .addComponent(txtCantLineas, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(btnIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        Area area = (Area) this.lstArea.getSelectedItem();
        try {
            String input = this.txtCantLineas.getText().trim();
            int cant = Integer.parseInt(input);
            if (cant > 0) {
                try
                {
                    MonitorView monitor = new MonitorView(null, false, area, cant);
                    monitor.setVisible(true);
                    this.dispose();
                }
                catch (AtencionException e){
                    JOptionPane.showMessageDialog(this, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Valor debe ser mayor a 0", "Error", JOptionPane.ERROR_MESSAGE);
            }

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Valor invalido para cantidad de lineas", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnIniciarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciar;
    private javax.swing.JLabel lblArea;
    private javax.swing.JLabel lblCantLineas;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JComboBox<Area> lstArea;
    private javax.swing.JTextField txtCantLineas;
    // End of variables declaration//GEN-END:variables
}
