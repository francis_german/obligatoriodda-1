package VistaSwing;

import Controlador.DerivarNumeroController;
import Controlador.Interfaces.IAtenderView;
import Controlador.Interfaces.IDerivarView;
import Excepciones.AtencionException;
import Excepciones.DerivacionException;
import Modelo.Area;
import Modelo.Puesto;
import Modelo.Sector;
import Utilidades.Utilidades;
import java.awt.event.ItemEvent;
import javax.swing.DefaultComboBoxModel;

public class DerivarNumeroView extends javax.swing.JDialog implements IDerivarView {

    private final DerivarNumeroController controlador;
    private boolean estaEsperando;

    public DerivarNumeroView(java.awt.Frame parent, boolean modal, Puesto puestoOrigen, String descr, IAtenderView vAtender) throws AtencionException {
        super(parent, modal);
        initComponents();
        this.controlador = new DerivarNumeroController(this, puestoOrigen, descr, vAtender);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cbAreas = new javax.swing.JComboBox();
        cbSectores = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnConfirmar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        lblNombreDestino = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblCantDeriv = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblLibre = new javax.swing.JLabel();
        btnCerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                formMouseDragged(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });

        jPanel1.setBackground(javax.swing.UIManager.getDefaults().getColor("InternalFrame.activeTitleBackground"));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        cbAreas.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbAreasItemStateChanged(evt);
            }
        });

        cbSectores.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbSectoresItemStateChanged(evt);
            }
        });

        jLabel1.setText("Areas");

        jLabel2.setText("Sectores");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("DERIVAR ATENCIÓN");

        btnConfirmar.setText("Confirmar");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmarActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel5.setText("Nombre Trabajador:");

        lblNombreDestino.setText("NOMBRE");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel7.setText("Cantidad Derivaciones:");

        lblCantDeriv.setText("CANTIDAD");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel9.setText("Esta Libre:");

        lblLibre.setText("ESTADO");

        btnCerrar.setBackground(new java.awt.Color(255, 51, 51));
        btnCerrar.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(255, 255, 255));
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 79, Short.MAX_VALUE)
                                .addComponent(cbAreas, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbSectores, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel5)
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCantDeriv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblNombreDestino, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblLibre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(22, 22, 22))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCerrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnConfirmar)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbAreas, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbSectores, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(39, 39, 39)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lblNombreDestino))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblCantDeriv))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lblLibre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 107, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnConfirmar)
                    .addComponent(btnCerrar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbAreasItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbAreasItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            Area a = (Area) this.cbAreas.getSelectedItem();
            try {
                this.controlador.cargarSectores(a);
            } catch (AtencionException ex) {
                Utilidades.mostrarError(ex.getMessage(), this);
            }
        }
    }//GEN-LAST:event_cbAreasItemStateChanged

    int xx = 0;
    int yy = 0;

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        // TODO add your handling code here:
        xx = evt.getX();
        yy = evt.getY();
    }//GEN-LAST:event_formMousePressed

    private void formMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseDragged
        // TODO add your handling code here:
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xx, y - yy);
    }//GEN-LAST:event_formMouseDragged

    private void btnConfirmarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmarActionPerformed
        try {
            Sector s = (Sector) cbSectores.getSelectedItem();
            this.controlador.derivar();
        } catch (AtencionException | DerivacionException ex) {
            Utilidades.mostrarError(ex.getMessage(), this);
        }
    }//GEN-LAST:event_btnConfirmarActionPerformed

    private void cbSectoresItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbSectoresItemStateChanged
        Sector s = (Sector) cbSectores.getSelectedItem();
        this.controlador.puestosDerivacion(s);
    }//GEN-LAST:event_cbSectoresItemStateChanged

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        if (estaEsperando) {
            Utilidades.mostrarError("El puesto destino aun no ha respondido.", this);
        } else {
            this.controlador.cerrar();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnConfirmar;
    private javax.swing.JComboBox cbAreas;
    private javax.swing.JComboBox cbSectores;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblCantDeriv;
    private javax.swing.JLabel lblLibre;
    private javax.swing.JLabel lblNombreDestino;
    // End of variables declaration//GEN-END:variables

    @Override
    public void cargarAreas(Area[] areas) {
        DefaultComboBoxModel<Area> m = new DefaultComboBoxModel<>();
        for (Area area : areas) {
            m.addElement(area);
        }
        cbAreas.setModel(m);
    }

    @Override
    public void cargarSectores(Sector[] sectores) {
        DefaultComboBoxModel<Sector> m = new DefaultComboBoxModel<>();
        for (Sector s : sectores) {
            m.addElement(s);
        }
        cbSectores.setModel(m);
    }

    @Override
    public void mostrarDestino(Puesto p) {

        String msgVacio = "No hay puestos disponibles";
        String nombre = msgVacio;
        String cantDeriv = msgVacio;
        String libre = msgVacio;

        if (p != null) {
            nombre = p.getTrabajador().getNombreCompleto();
            cantDeriv = p.cantNroDerivados() + "";
            libre = p.estaLibre() ? "LIBRE" : "OCUPADO";
        }

        this.lblCantDeriv.setText(cantDeriv);
        this.lblNombreDestino.setText(nombre);
        this.lblLibre.setText(libre);
    }

    @Override
    public void cerrar() {
        this.dispose();
    }

    @Override
    public void marcarEsperando(boolean a) {
        this.estaEsperando = a;
    }

    @Override
    public void mensajeError(String msg) {
        Utilidades.mostrarError(msg, this);
    }

    @Override
    public void mensajeInfo(String msg) {
        Utilidades.mostrarMsg(msg, this);
    }

    @Override
    public Sector sectorSeleccionado() {
        return (Sector) cbSectores.getSelectedItem();
    }

}
