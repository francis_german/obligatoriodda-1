package VistaSwing;

import Controlador.Interfaces.IPedirNumeroView;
import Controlador.PedirNumeroController;
import Excepciones.AtencionException;
import Modelo.Area;
import Modelo.Atencion;
import Modelo.Sector;
import Utilidades.Utilidades;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class PedirNumeroView extends javax.swing.JDialog implements IPedirNumeroView {

    private final PedirNumeroController controlador;

    public PedirNumeroView(java.awt.Frame parent, boolean modal, Area area) throws AtencionException {
        super(parent, modal);
        initComponents();
        //El controlador lleva el area, asi no cargo a la vista con el atributo
        this.controlador = new PedirNumeroController(this, area.getId());

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtTitulo = new javax.swing.JTextField();
        lblSectores = new javax.swing.JLabel();
        lstSectores = new javax.swing.JComboBox<>();
        btnIniciar = new javax.swing.JButton();
        lblCliente = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtTitulo.setEditable(false);
        txtTitulo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTitulo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTitulo.setText("NOMBRE DEL AREA");

        lblSectores.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblSectores.setText("Elija el Sector");

        lstSectores.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnIniciar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnIniciar.setText("Pedir Número");
        btnIniciar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        btnIniciar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });

        lblCliente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblCliente.setText("Nro.de Cliente");

        txtCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtCliente.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Fotos/atencion.jpg"))); // NOI18N
        jLabel1.setText("Elija aqui su número");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTitulo)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCliente))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblSectores, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lstSectores, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSectores)
                    .addComponent(lstSectores, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCliente)
                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnIniciar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        Sector sector = (Sector) this.lstSectores.getSelectedItem();
        try {
            int nroCliente = Integer.parseInt(this.txtCliente.getText().trim());
            this.controlador.pedirNumero(sector.getId(), nroCliente);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Valor invalido para Nro. de Cliente", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnIniciarActionPerformed

    @Override
    public void mostrarNroAsignado(Atencion atencion, String nombreSector) {
        if (atencion != null) {
            String msj = "Número: " + atencion.getNumero() + "\nCliente: " + atencion.getCliente().getNombreCompleto()
                    + "\nFecha de Solicitud: " + Utilidades.FechaToString(atencion.getFchHraCreado()) + "\nSector: "
                    + nombreSector
                    + "\nEstado: " + atencion.EstadoToString();
            JOptionPane.showMessageDialog(this, msj, "Nro. Asignado", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    @Override
    public void mostrarError(int codigo, String mensaje) {
        String msj = "Se a producido el error " + codigo + " - " + mensaje;
        JOptionPane.showMessageDialog(this, msj, "Error", JOptionPane.ERROR_MESSAGE);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblCliente;
    private javax.swing.JLabel lblSectores;
    private javax.swing.JComboBox<Sector> lstSectores;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JTextField txtTitulo;
    // End of variables declaration//GEN-END:variables

    @Override
    public void mostrarNombreArea(String msg) {
        this.txtTitulo.setText(msg);
    }

    @Override
    public void cargarSectores(Sector[] sectores) {
        DefaultComboBoxModel<Sector> m = new DefaultComboBoxModel<>();
        for (Sector s : sectores) {
            m.addElement(s);
        }
        lstSectores.setModel(m);
    }

}
