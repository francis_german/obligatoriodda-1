
package persistencia;

import Modelo.Area;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MapeadorArea implements Mapeador{
    
    private Area area;

    public MapeadorArea() {
    }

    public MapeadorArea(Area area) {
        this.area = area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
    
    @Override
    public int getOid() {
        return area.getId();
    }

    @Override
    public void setOid(int id) {
        area.setId(id);
    }

    @Override
    public ArrayList<String> getSqlsInsertar() {
        
        ArrayList<String> sqls = new ArrayList();
        String sql = "INSERT INTO Areas (idArea, nombre) values (" + area.getId() + ", '" + area.getNombre() + "')"; 
        sqls.add(sql);
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsActualizar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add("UPDATE Areas set nombre='" + area.getNombre()+ 
                 "' WHERE idArea=" + getOid());
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsBorrar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add("DELETE FROM Areas WHERE idArea=" + getOid());
        return sqls;
    }

    @Override
    public String getSqlSeleccionar() {
        return "SELECT * FROM Areas";
    }
    
    @Override
    public void crearNuevo() {
        area = new Area();
    }

    @Override
    public void leer(ResultSet rs) throws SQLException {
        area.setId(rs.getInt("idArea"));
        area.setNombre(rs.getString("nombre"));
    }

    @Override
    public Object getObjeto() {
        return area;
    }

    @Override
    public void leerComponente(ResultSet rs) throws SQLException {
        //El área no tiene componentes extras.
    }
    
}
