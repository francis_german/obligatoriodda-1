
package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Persistencia {
    
    private static Persistencia intancia = new Persistencia();
    
    private BaseDatos base = BaseDatos.getInstancia();
    
    public static Persistencia getInstancia() {
        return intancia;
    }
    
    private Persistencia() {
        
    }
   
    public int proximoOid(String clave){
        try {
            ResultSet rs = base.consultar("SELECT ultNro FROM Numeros where tipo = '" + clave + "'");

            if(!rs.next()){
                System.out.println("Falta cargar oid inicial!");
                return -1;
            }
            int oid = rs.getInt("ultNro");
            String sql = "UPDATE Numeros SET ultNro=" + (oid+1) + " where tipo = '" + clave + "'";
            base.modificar(sql);
            return oid;
        } catch (SQLException ex) {
            System.out.println("Error al obtener el oid:" + ex.getMessage());
            return -1;
        }
    }
    
    public void guardar(Mapeador obj, String clave) {
        if(obj.getOid()==0){
            insertar(obj, clave);
        }else{
            actualizar(obj);
        }
    }
    
    private void insertar(Mapeador obj, String clave) {
        int oid = proximoOid(clave);
        obj.setOid(oid);
        try{
            if(!base.transaccion(obj.getSqlsInsertar())){
                obj.setOid(0);   
                System.out.println("Error al insertar");
            }
        }
        catch(Exception e){
            //throw(e);
        }
    }
    
    private void actualizar(Mapeador obj) {
        try{
            ArrayList<String> sqls = obj.getSqlsActualizar();

            if(!base.transaccion(sqls)){
                System.out.println("Error al actualizar");
            }
        }
        catch(Exception e){
            String msj = e.getMessage();
            //throw(e);
        }
    }
    
    public void borrar(Mapeador obj) {
        try{
            ArrayList<String> sqls = obj.getSqlsBorrar();

            if(!base.transaccion(sqls)){
                System.out.println("Error al borrar");
            }else{
                obj.setOid(0);
            }
        }
        catch(Exception e){
            //throw(e);
        }
    }
    
    public ArrayList<Object> todos(Mapeador obj){
        return buscar(obj,null);
    }
    
    public ArrayList<Object> buscar(Mapeador obj, String filtro) {
        
        ArrayList<Object> lista = new ArrayList();
        
        String sql = obj.getSqlSeleccionar();
        if(filtro!=null) sql += " WHERE " + filtro;
        
        try {
            ResultSet rs = base.consultar(sql);
            if(rs==null) return null;

            int oidActual, oidAnterior = -1;
            while(rs.next()){
                oidActual = rs.getInt(1); //debe llamarse asi en la tabla
                if(oidActual!=oidAnterior){ //cambió el oid
                   oidAnterior = oidActual;
                   obj.crearNuevo(); //Crear un nuevo objeto / Compuesto
                   obj.setOid(oidActual); 
                   obj.leer(rs); //Lee los datos del objeto / Compuesto
                   lista.add(obj.getObjeto()); //lo guardo
                }
                obj.leerComponente(rs);
            }
        } catch (SQLException ex) {
            System.out.println("Error al buscar:" + ex.getMessage());
        }
        
        return lista;
    }
    
}
