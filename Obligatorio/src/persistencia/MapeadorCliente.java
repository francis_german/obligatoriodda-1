
package persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Cliente;
import Costos.*;


public class MapeadorCliente implements Mapeador{
    
    private Cliente cliente;

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public int getOid() {
        return cliente.getId();
    }

    @Override
    public void setOid(int id) {
        cliente.setId(id);
    }

    public MapeadorCliente() {
    }

    public MapeadorCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public ArrayList<String> getSqlsInsertar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add( "INSERT INTO Clientes (idCliente, nombre, email, documento, tipoCosto) "
                                      + "values (" + getOid() + ",'" + cliente.getNombreCompleto()+ "','" +
                                                  cliente.getMail()+ "', '" + cliente.getDocumento() + "'," +
                                                  cliente.getCosto().codigo() + ")");
        return sqls;
        
    }

    @Override
    public ArrayList<String> getSqlsActualizar() {
        ArrayList<String> sqls =new ArrayList();
        sqls.add("UPDATE Clientes SET nombre='" + cliente.getNombreCompleto() + "', email='" + cliente.getMail() 
                                      + "', documento ='" + cliente.getDocumento() + "', tipoCosto = " 
                                      + cliente.getCosto().codigo() + " WHERE idCliente=" + getOid());
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsBorrar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add( "DELETE FROM Clientes WHERE idCliente = " + getOid());
        return sqls;
    }

    @Override
    public String getSqlSeleccionar() {
        return "SELECT * FROM Clientes ";
    }

    @Override
    public void crearNuevo() {
        cliente = new Cliente();
    }

    @Override
    public void leer(ResultSet rs) throws SQLException {
        cliente.setId(rs.getInt("idCliente"));
        cliente.setNombreCompleto(rs.getString("nombre"));        
        cliente.setMail(rs.getString("email"));        
        cliente.setDocumento(rs.getString("documento"));
        cliente.setCosto(this.tipoCosto(rs.getInt("tipoCosto")));
    }

    @Override
    public Object getObjeto() {
        return cliente;
    }

    @Override
    public void leerComponente(ResultSet rs) throws SQLException {
        //no hago nada
    }
    
    private ICosto tipoCosto(int tipo){
        switch (tipo){
            case 1:
                return new CostoFijo();
            case 2:
                return new Especial();
            case 3:
                return new Exonerado();
            case 4:
                return new Gestores();
            default:
                return null;
        }
    }
}
