
package persistencia;

import Modelo.Atencion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Cliente;
import Modelo.Puesto;
import Modelo.Sector;
import Modelo.Trabajador;
import java.util.Calendar;
import java.util.TimeZone;

public class MapeadorAtencion implements Mapeador{
    
    private Atencion atencion;

    public void setAtencion(Atencion atencion) {
        this.atencion = atencion;
    }

    @Override
    public int getOid() {
        return atencion.getId();
    }

    @Override
    public void setOid(int id) {
        atencion.setId(id);
    }

    public MapeadorAtencion() {
    }

    public MapeadorAtencion(Atencion atencion) {
        this.atencion = atencion;
    }

    @Override
    public ArrayList<String> getSqlsInsertar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add( "INSERT INTO Atenciones (idAtencion, idCliente, numero, fchHraCreado, estado)"
                                       + "values (" + getOid() + "," + 
                                                  atencion.getCliente().getId() + "," +
                                                  atencion.getNumero()+ ",'" + 
                                                  new java.sql.Timestamp(atencion.getFchHraCreado().getTime()) + "','" + 
                                                  atencion.getEstado().toString() + "')");
        return sqls;
        
    }

    @Override
    public ArrayList<String> getSqlsActualizar() {
        ArrayList<String> sqls =new ArrayList();
        String sql = "UPDATE Atenciones SET ";
        if(atencion.getFchHraInicio() != null) sql = sql + "fchHraInicio= '" + new java.sql.Timestamp(atencion.getFchHraInicio().getTime()) + "',";
        if(atencion.getFchHraFin() != null){
            double costo = atencion.getCliente().getCosto().calcularCosto(atencion);
            sql = sql + "fchHraFin= '" + new java.sql.Timestamp(atencion.getFchHraFin().getTime()) + "'," +
                  "costo = " + costo + "," ;
        }
        if(atencion.getPuesto() != null) sql = sql + "idPuesto= " + atencion.getPuesto().getId() + ",";
        if(atencion.getTrabInicio() != null) sql = sql + "idTrabInicio = " + atencion.getTrabInicio().getId() + ",";
        if(atencion.getTrabFin() != null) sql = sql + "idTrabFin = " + atencion.getTrabFin().getId() + ",";
        if(atencion.getDescripcion() != null) sql = sql + "descripcion = '" + atencion.getDescripcion() + "',";
        if(atencion.getEstado().toString() != null) sql = sql + "estado = '" + atencion.getEstado().toString() + "',";
        sql = sql.substring(0, sql.length() - 1);
        sql = sql + " WHERE idAtencion = " + getOid();
        sqls.add(sql);
        return sqls;
        
    }

    @Override
    public ArrayList<String> getSqlsBorrar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add( "DELETE FROM Atenciones WHERE idAtencion = " + getOid());
        return sqls;
    }

    @Override
    public String getSqlSeleccionar() {
        return "SELECT * FROM Atenciones ";
    }

    @Override
    public void crearNuevo() {
        atencion = new Atencion();
    }

    @Override
    public void leer(ResultSet rs) throws SQLException {
        atencion.setId(rs.getInt("idAtencion"));
        TimeZone zona = TimeZone.getTimeZone("GMT-3:00");
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(zona);
        atencion.setFchHraCreado(rs.getTimestamp("fchHraCreado", cal));        
        atencion.setNumero(rs.getInt("numero"));        
        atencion.setFchHraInicio(rs.getTimestamp("fchHraInicio", cal));        
        atencion.setFchHraFin(rs.getDate("fchHraFin", cal));
        atencion.setDescripcion(rs.getString("descripcion"));
        atencion.setEstado(Atencion.EstadoAtencion.valueOf(rs.getString("estado")));
//        this.leerComponente(rs);
    }

    @Override
    public Object getObjeto() {
        return atencion;
    }

    @Override
    public void leerComponente(ResultSet rs) throws SQLException {
        Cliente cliente = new Cliente();
        Trabajador trab = new Trabajador();
        cliente.setId(rs.getInt("idCliente"));
        this.atencion.setCliente(cliente);
        int idTrab = rs.getInt("idTrabInicio");
        trab.setId(idTrab);
        if (idTrab > 0) this.atencion.setTrabInicio(trab);
        trab = new Trabajador();
        idTrab = rs.getInt("idTrabFin");
        trab.setId(idTrab);
        if (idTrab > 0) this.atencion.setTrabFin(trab);
//        MapeadorCliente mapCliente = new MapeadorCliente();
//        String filtro = "idCliente = " + rs.getInt("idCliente");
//        BaseDatos bd = BaseDatos.getInstancia();
//        try{
//            Persistencia inst = Persistencia.getInstancia();
//            bd.conectarse();
//            cliente = (Cliente)inst.buscar(mapCliente, filtro).get(0);
//            this.atencion.setCliente(cliente);
//
//            int idPuesto = rs.getInt("idPuesto");
//            if (idPuesto > 0){
//                Puesto puesto = new Puesto();
//                MapeadorPuesto mapPuesto = new MapeadorPuesto(puesto);
//                filtro = "idPuesto = " + idPuesto;
//                puesto = (Puesto)inst.buscar(mapPuesto, filtro).get(0);
//                this.atencion.setPuesto(puesto);
//            }
//
//            MapeadorTrabajador mapTrabajador = new MapeadorTrabajador();
//            Trabajador trabajador = new Trabajador();
//            int idTrab = rs.getInt("idTrabInicio");
//            if (idTrab > 0){
//                filtro = "idTrabajador = " + idTrab;
//                trabajador = (Trabajador)inst.buscar(mapTrabajador, filtro).get(0);
//                this.atencion.setTrabInicio(trabajador);
//            }
//
//            idTrab = rs.getInt("idTrabFin");
//            if (idTrab > 0){
//                filtro = "idTrabajador = " + idTrab;
//                trabajador = (Trabajador)inst.buscar(mapTrabajador, filtro).get(0);
//                this.atencion.setTrabFin(trabajador);
//            }
//        }
//        catch(SQLException e){
//            
//        }
//        finally{
//            bd.desconectar();
//        }
    }
}
