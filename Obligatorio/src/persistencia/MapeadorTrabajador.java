
package persistencia;

import Modelo.Sector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import Modelo.Trabajador;

public class MapeadorTrabajador implements Mapeador{
    
    private Trabajador trabajador;

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    @Override
    public int getOid() {
        return trabajador.getId();
    }

    @Override
    public void setOid(int id) {
        trabajador.setId(id);
    }

    public MapeadorTrabajador() {
    }

    public MapeadorTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    @Override
    public ArrayList<String> getSqlsInsertar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add( "INSERT INTO Trabajadores (idTrabajador, nombre, documento, pass, idSector)"
                                    + " values (" + getOid() + ",'" + 
                                                 trabajador.getNombreCompleto()+ "','" +
                                                 trabajador.getDocumento() + "', '" +
                                                 trabajador.getPass()+ "', " + 
                                                 trabajador.getSector().getId() + ")");
        
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsActualizar() {
        ArrayList<String> sqls =new ArrayList();
        sqls.add("UPDATE Clientes SET nombre='" + trabajador.getNombreCompleto() + "', " + 
                                     "documento ='" + trabajador.getDocumento()+ "', " + 
                                     "pass ='" + trabajador.getPass()+ "', " +
                                     "idSector = " + trabajador.getSector().getId() + 
                                     "' WHERE oid=" + getOid());
        return sqls;
        
    }

    @Override
    public ArrayList<String> getSqlsBorrar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add( "DELETE FROM Clientes WHERE idCliente = " + getOid());
        return sqls;
    }

    @Override
    public String getSqlSeleccionar() {
        return "SELECT * FROM Trabajadores ";
    }

    @Override
    public void crearNuevo() {
        trabajador = new Trabajador();
    }

    @Override
    public void leer(ResultSet rs) throws SQLException {
        trabajador.setId(rs.getInt("idTrabajador"));
        trabajador.setNombreCompleto(rs.getString("nombre"));        
        trabajador.setDocumento(rs.getString("documento"));        
        trabajador.setPass(rs.getString("pass"));
        //this.leerComponente(rs);
    }

    @Override
    public Object getObjeto() {
        return trabajador;
    }

    @Override
    public void leerComponente(ResultSet rs) throws SQLException {
        Sector sector = new Sector();
        sector.setId(rs.getInt("idSector"));
        this.trabajador.setSector(sector);
//        MapeadorSector mapSector = new MapeadorSector(sector);
//        String filtro = "idSector = " + rs.getInt("idSector");
//        BaseDatos bd = BaseDatos.getInstancia();
//        try{
//            bd.conectarse();
//            sector = (Sector)Persistencia.getInstancia().buscar(mapSector, filtro).get(0);
//            this.trabajador.setSector(sector);
//        }
//        catch(SQLException e){
//            throw(e);
//        }
//        finally{
//            bd.desconectar();
//        }
    }
}
