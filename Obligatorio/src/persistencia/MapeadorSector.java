
package persistencia;

import Modelo.Area;
import Modelo.Sector;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MapeadorSector implements Mapeador{
    
    private Sector sector;

    public MapeadorSector() {
    }

    public MapeadorSector(Sector sector) {
        this.sector = sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }
    
    @Override
    public int getOid() {
        return sector.getId();
    }

    @Override
    public void setOid(int id) {
        sector.setId(id);
    }

    @Override
    public ArrayList<String> getSqlsInsertar() {
        
        ArrayList<String> sqls = new ArrayList();
        String sql = "INSERT INTO Sectores (idSector, idArea, nombre, ultNroPuesto, ultNroAtencion)"
                                       + " values (" + sector.getId() + "," + sector.getArea().getId()+ ", '" 
                                                     + sector.getNombre() + "', " + sector.getUltNumeroPuesto() + ", "
                                                     + sector.getUltNroAtencion() + ")"; 
        sqls.add(sql);
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsActualizar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add("UPDATE Sectores set nombre='" + sector.getNombre() + "', " + 
                              "ultNroPuesto = " + sector.getUltNumeroPuesto() + ", " +
                            "ultNroAtencion = " + sector.getUltNroAtencion()  +
                 " WHERE idSector=" + getOid());
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsBorrar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add("DELETE FROM Sectores WHERE idSector=" + getOid());
        return sqls;
    }

    @Override
    public String getSqlSeleccionar() {
        return "SELECT s.idSector, s.idArea, s.nombre, s.ultNroPuesto, s.ultNroAtencion " +
               "FROM Sectores s";
    }
    
    @Override
    public void crearNuevo() {
        sector = new Sector();
    }

    @Override
    public void leer(ResultSet rs) throws SQLException {
        sector.setId(rs.getInt("idSector"));
        sector.setNombre(rs.getString("nombre"));
        sector.setUltNumeroPuesto(rs.getInt("ultNroPuesto"));
        sector.setUltNroAtencion(rs.getInt("ultNroAtencion"));
        Area area = new Area();
        area.setId(rs.getInt("idArea"));
        sector.setArea(area);
        //this.leerComponente(rs);
    }

    @Override
    public Object getObjeto() {
        return sector;
    }

    @Override
    public void leerComponente(ResultSet rs) throws SQLException {
//        Area area = new Area();
//        area.setId(rs.getInt("idArea"));
//        MapeadorArea mapArea = new MapeadorArea(area);
//        String filtro = "idArea = " + rs.getInt("idArea");
//        area = (Area)Persistencia.getIntancia().buscar(mapArea, filtro).get(0);
//        area.cargarSectores();
//        this.sector.setArea(area);
    }
    
}
