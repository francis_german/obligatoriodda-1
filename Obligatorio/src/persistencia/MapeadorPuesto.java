
package persistencia;

import Modelo.Area;
import Modelo.Atencion;
import Modelo.Sector;
import Modelo.Puesto;
import Modelo.Trabajador;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.Duration;

public class MapeadorPuesto implements Mapeador{
    
    private Puesto puesto;

    public MapeadorPuesto() {
    }

    public MapeadorPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    public void setPuesto(Puesto puesto) {
        this.puesto = puesto;
    }
    
    @Override
    public int getOid() {
        return puesto.getId();
    }

    @Override
    public void setOid(int id) {
        puesto.setId(id);
    }

    @Override
    public ArrayList<String> getSqlsInsertar() {
        
        ArrayList<String> sqls = new ArrayList();
        String sql = "INSERT INTO Puestos (idPuesto, nroPuesto, idSector) "
                                          + "values (" + puesto.getId() + "," 
                                                       + puesto.getNroPuesto() + "," 
                                                       + puesto.getSector().getId()+ ")";
        sqls.add(sql);
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsActualizar() {
        ArrayList<String> sqls = new ArrayList();

        String sql = "UPDATE Puestos SET nroPuesto = " + puesto.getNroPuesto() 
                                     + ", idSector = " + puesto.getSector().getId() + ",";
        if(puesto.getTrabajador() != null && puesto.getTrabajador().getId() > 0){
            sql = sql + "idTrabajador= " + puesto.getTrabajador().getId() + ",";
        }
        else{
            sql = sql + "idTrabajador= " + null + ",";
        }
        if(puesto.getAtencionActual() != null && puesto.getAtencionActual().getId() > 0){
            sql = sql + "atencionActual= " + puesto.getAtencionActual().getId() + ",";
        }
        else{
            sql = sql + "atencionActual= " + null + ",";
        }
        if(puesto.getNumerosAsignados() > 0){
            sql = sql + "nrosAsignados= " + puesto.getNumerosAsignados() + ",";
        }
        else{
            sql = sql + "nrosAsignados= " + 0 + ",";
        }
        //if(puesto.getTiempo() != null) sql = sql + "tiempo = " + puesto.getTiempo() + ",";
        sql = sql.substring(0, sql.length() - 1);
        sql = sql + " WHERE idPuesto=" + getOid();
        sqls.add(sql);
        
        return sqls;
    }

    @Override
    public ArrayList<String> getSqlsBorrar() {
        ArrayList<String> sqls = new ArrayList();
        sqls.add("DELETE FROM Puestos WHERE idPuesto=" + getOid());
        return sqls;
    }

    @Override
    public String getSqlSeleccionar() {
        return "SELECT * FROM Puestos";
    }
    
    @Override
    public void crearNuevo() {
        puesto = new Puesto();
    }

    @Override
    public void leer(ResultSet rs) throws SQLException {
        puesto.setId(rs.getInt("idPuesto"));
        puesto.setNroPuesto(rs.getInt("nroPuesto"));
        int nros = rs.getInt("nrosAsignados");
        if (puesto.getNumerosAsignados() < nros) puesto.setNumerosAsignados(nros);
        puesto.setTiempo(new Duration(rs.getTime("tiempo")));
        //this.leerComponente(rs);
    }

    @Override
    public Object getObjeto() {
        return puesto;
    }

    @Override
    public void leerComponente(ResultSet rs) throws SQLException {
        Trabajador trab = new Trabajador();
        trab.setId(rs.getInt("idTrabajador"));
        this.puesto.setTrabajador(trab);
        Atencion ate = new Atencion();
        ate.setId(rs.getInt("atencionActual"));
        this.puesto.setAtencion(ate);
//        Sector sector = new Sector();
//        MapeadorSector mapSector = new MapeadorSector(sector);
//        String filtro = "idSector = " + rs.getInt("idSector");
//        BaseDatos bd = BaseDatos.getInstancia();
//        try{
//            Persistencia inst = Persistencia.getInstancia();
//            bd.conectarse();
//            sector = (Sector)inst.buscar(mapSector, filtro).get(0);
//            bd.desconectar();
//            this.puesto.setSector(sector);
//
//            int idtrab = rs.getInt("idTrabajador");
//            if(idtrab > 0){
//                Trabajador trabajador = new Trabajador();
//                MapeadorTrabajador mapTrabajador = new MapeadorTrabajador();
//                filtro = "idTrabajador = " + idtrab;
//                bd.conectarse();
//                trabajador = (Trabajador)inst.buscar(mapTrabajador, filtro).get(0);
//                bd.desconectar();
//                this.puesto.setTrabajador(trabajador);
//            }
//
//            int idaten = rs.getInt("atencionActual");
//            if (idaten > 0){
//                Atencion atencion = new Atencion();
//                bd.conectarse();
//                MapeadorAtencion mapAtencion = new MapeadorAtencion();
//                filtro = "idAtencion = " + idaten;
//                ArrayList<Atencion> atenciones = (ArrayList<Atencion>)(List)inst.buscar(mapAtencion, filtro);
//                bd.desconectar();
//                if (atenciones != null && atenciones.size() > 0){
//                    atencion = atenciones.get(0);
//                    atencion.setPuesto(puesto);
//                    this.puesto.setAtencion(atencion);
//                }
//            }
//        }
//        catch(SQLException e){
//            
//        }
//        finally{
//            bd.desconectar();
//        }
    }
}
