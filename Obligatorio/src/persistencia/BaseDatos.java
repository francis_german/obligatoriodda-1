/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alumnoFI
 */
public class BaseDatos {
    
    private static BaseDatos instancia = new BaseDatos();
    private Connection conexion;
    private Statement stmt;

    public static BaseDatos getInstancia() {
        return instancia;
    }
    
    private BaseDatos() {
    }
    
    public void conectarse() throws SQLException{
        try{
//            InputStream is;
//            Properties prop = new Properties();
//            String archivo = "Configuracion/config.properties";
//            is = getClass().getClassLoader().getResourceAsStream(archivo);
//            prop.load(is);
//            
//            String driver = prop.getProperty("driver");
//            String url = prop.getProperty("url");
//            String usr = prop.getProperty("usr");
//            String pass = prop.getProperty("pass");
            
            String driver = "com.mysql.cj.jdbc.Driver";
            String url = "jdbc:mysql://localhost:3306/obligatoriodda?useUnicode=true" +
                         "&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
            String usr = "root";
            String pass = "";
            
            if (this.conexion == null || this.conexion.isClosed()){
                Class.forName(driver); //"carga la clase del driver"
                conexion = DriverManager.getConnection(url, usr, pass); //se conecta
                stmt = conexion.createStatement();

                System.out.println("CONECTADO!");
            }
        }catch(Exception e){
            System.out.println("Error al conectar:" + e.getMessage());
            if (!this.conexion.isClosed()) this.desconectar();
        }
    }
    
    public void desconectar() {
        try {
            if (!conexion.isClosed()) conexion.close();
            
            System.out.println("DESCONECTADO!");
        } catch (SQLException ex) {
            String msj="Error";
        }
    }
    
    //Para insert, update, delete
    public int modificar(String sql){
        try {
            return stmt.executeUpdate(sql);
        } catch (SQLException ex) {
            System.out.println("Error al modificar:" + ex.getMessage());
            System.out.println("SQL:" + sql);
            return -1;
        }
    }
    
    //Para SELECT
    public ResultSet consultar(String sql){
        try {
            return stmt.executeQuery(sql);
        } catch (SQLException ex) {
            System.out.println("Error al consultar:" + ex.getMessage());
            System.out.println("SQL:" + sql);
            return null;
        }
    }
    
    public boolean transaccion(ArrayList<String> sqls){
        try{
            conexion.setAutoCommit(false); //BEGIN T
            for(String sql:sqls){
                int r = modificar(sql);
                if(r==-1){ //fallo
                    conexion.rollback();
                    return false;
                }
            }
            conexion.commit();
            return true;
        }catch(SQLException e){
            System.out.println("Error al ejecutar transaccion:"  + e.getMessage());
            return false;
        }finally{
            try {
                conexion.setAutoCommit(true); //END T
            } catch (SQLException ex) {         }
        }
    }
}
