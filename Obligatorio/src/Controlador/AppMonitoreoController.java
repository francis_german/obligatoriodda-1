package Controlador;

import Controlador.Interfaces.IAppMonitoreo;
import Excepciones.AtencionException;
import Modelo.Area;
import Modelo.Fachada;

public class AppMonitoreoController {

    private final IAppMonitoreo vista;
    private final Fachada modelo;

    public AppMonitoreoController(IAppMonitoreo vista) throws AtencionException {
        this.vista = vista;
        this.modelo = Fachada.getSistema();
    }

    public void cargarListaAreas() {
        try{
            Area[] lista = this.modelo.pidoAreas();
            vista.cargoAreas(lista);
        }
        catch(AtencionException e){
            
        }
    }
}
