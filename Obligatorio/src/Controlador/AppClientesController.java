package Controlador;

import Controlador.Interfaces.IAppClientes;
import Excepciones.AtencionException;
import Modelo.Area;
import Modelo.Fachada;

public class AppClientesController {
    
    private final IAppClientes vista;
    private final Fachada modelo;
    
    public AppClientesController(IAppClientes vista) throws AtencionException {
        this.vista = vista;
        this.modelo = Fachada.getSistema();
    }
    
    public void cargarListaAreas() {
        try{
            Area[] lista = this.modelo.pidoAreas();
            vista.cargoAreas(lista);
        }
        catch(AtencionException e){
            
        }
    }
}
