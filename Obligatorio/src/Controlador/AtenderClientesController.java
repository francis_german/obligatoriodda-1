package Controlador;

import Controlador.Interfaces.IAtenderView;
import Excepciones.AtencionException;
import Modelo.Atencion;
import Modelo.DTO.AtencionDTO;
import Modelo.Fachada;
import Modelo.Puesto;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.Timer;

public class AtenderClientesController implements Observer {

    private final IAtenderView vista;
    private final Fachada modelo;

    //Atributo auxiliar
    private final Puesto puesto;

    //Auxiliar de puestos disponibles que puedo asignar una derivacion
    private ArrayList<Puesto> puestosParaDerivar;

    public AtenderClientesController(IAtenderView vista, Puesto p) throws AtencionException {
        this.vista = vista;
        this.puesto = p;
        this.modelo = Fachada.getSistema();
        this.modelo.agregarObserverAlPuesto(this.puesto, this);
        cargarInformacion();
    }

    private void cargarInformacion() {
        int nroPuesto = puesto.getNroPuesto();
        String nombreSector = puesto.getSector().getNombre().toUpperCase();
        String nombreArea = puesto.getSector().getArea().getNombre().toUpperCase();
        String info = "Area: " + nombreArea + " Sector: " + nombreSector;
        String titulo = puesto.getTrabajador().getNombreCompleto() + " atendiendo en puesto N° " + nroPuesto;

        this.vista.cargarInfo(titulo, info);

        if (this.puesto.getAtencionActual() == null) {
            this.vista.mostrarPuestoVacio();
        } else {
            this.vista.mostrarAtencion(this.puesto.getAtencionActual());
        }
        this.vista.actualizarEstadisticas(puesto);
    }

    public void logout() throws Exception {
        this.puesto.logout();
    }

    @Override
    public void update(Observable o, Object arg) {

        AtencionDTO a = (AtencionDTO) arg;

        boolean puestoVacio = a == null;
        boolean esDerivacion = a != null && a.getTipo() == AtencionDTO.Tipo.DERIVACION;

        if (!esDerivacion) {
            if (puestoVacio) {
                this.vista.mostrarPuestoVacio();
            } else {
                if (this.puesto.getAtencionActual() == null) {
                    this.vista.mostrarPuestoVacio();
                } else {
                    this.vista.mostrarAtencion(this.puesto.getAtencionActual());
                }
            }
            this.vista.actualizarEstadisticas(puesto);
        } else {
            //Si es una atencion derivada para preguntar
            AtencionDTO dto = (AtencionDTO) arg;

            if (dto != null && dto.isPregunta()) {

                String msg = puesto.getTrabajador().getNombreCompleto()
                        //                        + " Confirmar atencion de "
                        + "\nConfirmar DERIVACION"
                        + "\n**************************"
                        + "\nCliente: "
                        + dto.getAtencion().getCliente().getNombreCompleto()
                        + "\nDescripción: "
                        + dto.getAtencion().getDescripcion()
                        //                        + "\n que manda "
                        + "\n**************************"
                        + "\nOrigen"
                        + "\nUsuario: "
                        + dto.getAtencion().getPuesto().getTrabajador().getNombreCompleto()
                        + "\nArea: "
                        + dto.getAtencion().getPuesto().getSector().getArea().getNombre()
                        + "\nSector: "
                        + dto.getAtencion().getPuesto().getSector().getNombre();
//                        + "\n desde el AREA " + dto.getAtencion().getPuesto().getSector().getArea().getNombre()
//                        + "\n SECTOR " + dto.getAtencion().getPuesto().getSector().getNombre();

                //Bandera si el usuario eligio una opcion...
                boolean timer = false;
                this.timer(dto, 10000, timer);
                int res = this.vista.preguntarDerivacion(msg);
                timer = true;

                //Si es 0
                //No-Cancel es 1
                if (res == 0) {
                    dto.setAceptada(true);
                } else {
                    dto.setAceptada(false);
                }
                dto.setRespuesta(true);

                try {
                    this.modelo.notificarDerivacion(this.puesto, dto);
                } catch (AtencionException e) {

                }

                if (dto.isAceptada()) {

                    try {
                        this.modelo.recibirDerivacion(this.puesto, dto);
                    } catch (AtencionException ex) {
                        this.vista.mensajeError(ex.getMessage());
                    }
                }

            } else {
                this.vista.actualizarCantDerivados(this.puesto.cantNroDerivados());
            }
        }
    }

    private void timer(AtencionDTO dto, int timeout, boolean t) {
        Timer timer;
        timer = new Timer(timeout, (ActionEvent e) -> {
            if (!t) {

                dto.setAceptada(false);
                dto.setRespuesta(true);
                this.vista.cerrarPregunta();

                try {
                    this.modelo.notificarDerivacion(this.puesto, dto);
                } catch (AtencionException ex) {

                }

                if (dto.isAceptada()) {

                    try {
                        this.modelo.recibirDerivacion(this.puesto, dto);
                    } catch (AtencionException ex) {
                        this.vista.mensajeError(ex.getMessage());
                    }
                }
            }
        });
        timer.setRepeats(false);
        timer.setCoalesce(true);
        timer.start();
    }

    public void abrirAtencion() throws Exception {
        this.modelo.iniciarAtencion(this.puesto);
    }

    public void finalizarAtencion(String descripcion, boolean pedir) throws Exception {
        if (this.puesto.getDerivados().size() > 0 && !pedir) {
            throw new AtencionException(320, "Tiene Nros. Derivados Pendientes");
        } else {
            this.modelo.finalizoAtencion(puesto, descripcion, pedir);
        }
    }

    public Puesto puestoActual() {
        return this.puesto;
    }

    private void puedeDerivar() throws AtencionException {
        Atencion a = this.puesto.getAtencionActual();
        if (a == null) {
            throw new AtencionException(106, "El puesto no tiene atencion para derivar");
        }
        if (a.getEstado() != Atencion.EstadoAtencion.ABIERTA) {
            throw new AtencionException(105, "No puede derivar una atencion que no esta abierta");
        }
    }

    public void derivacion() throws AtencionException {
        this.puedeDerivar();
        this.vista.abrirDerivacion(puesto);
    }

}
