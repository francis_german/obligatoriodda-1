package Controlador;

import Controlador.Interfaces.IIngresoTrabajadorView;
import Excepciones.AtencionException;
import Excepciones.LoginException;
import Modelo.Fachada;
import Modelo.Puesto;
import Modelo.Trabajador;

public class IngresoTrabajadorController {

    private final IIngresoTrabajadorView vista;
    private final Fachada modelo;
    private Trabajador trabajador;

    public IngresoTrabajadorController(IIngresoTrabajadorView vista) throws AtencionException {
        this.vista = vista;
        this.modelo = Fachada.getSistema();
        this.trabajador = null;
    }

    public void validarLogin(String cedula, String password) throws LoginException, AtencionException {
        this.trabajador = this.modelo.validarLogin(cedula, password);
        if (this.trabajador != null){
            pedirPuestosLibres();
        }
        else
        {
            throw new LoginException(500, "Error de autenticación");
        }
    }
    
    public void pedirPuestosLibres(){
        try{
            Puesto[] puestos = modelo.pidoPuestosLibres(this.trabajador);
            String sector = this.trabajador.getSector().getNombre();
            this.vista.cargarPuestosLibres(puestos, sector);
        }catch (AtencionException e) {
            vista.mostrarError(e.getCodigo(), e.getMessage());
        }
    }

    public void asignarPuesto(Puesto puesto) {
        try{
            this.modelo.asignarPuesto(this.trabajador, puesto);
        }catch (AtencionException e) {
            vista.mostrarError(e.getCodigo(), e.getMessage());
        }
    }
}
