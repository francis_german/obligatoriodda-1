package Controlador;

import Controlador.Interfaces.IMonitorView;
import Excepciones.AtencionException;
import Modelo.Area;
import Modelo.Atencion;
import Modelo.Fachada;
import java.util.Observable;
import java.util.Observer;

public class MonitorViewController implements Observer {

    private final Fachada modelo;
    private final IMonitorView vista;
    private final Area area;

//<editor-fold defaultstate="collapsed" desc="** CONTRUCTOR Y METODOS **">
    public MonitorViewController(IMonitorView vista, int areaId) throws AtencionException {
        this.modelo = Fachada.getSistema();
        this.vista = vista;
        this.area = modelo.BuscarArea(areaId);
        this.modelo.agregarObserverAlArea(this.area, this);
        this.vista.mostrarTitulo(area.getNombre());
    }

    @Override
    public void update(Observable o, Object arg) {
        Atencion atencion = (Atencion) arg;
        if (atencion.getPuesto() != null) {
            if (atencion.getPuesto().getSector().getArea() == this.area) {
                String nro = atencion.getNumero() + "";
                if (atencion.getEstado() == Atencion.EstadoAtencion.DERIVADA) {
                    nro = "D-" + atencion.getNumero();
                }
                this.vista.actualizoMonitor(atencion, nro);
            }
        }
    }
//</editor-fold>    
}
