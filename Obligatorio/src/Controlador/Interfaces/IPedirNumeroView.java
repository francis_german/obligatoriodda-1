/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Interfaces;

import Modelo.*;

/**
 *
 * @author Francis
 */
public interface IPedirNumeroView {

    public void mostrarNroAsignado(Atencion a, String nombreSector);

    public void mostrarError(int codigo, String mensaje);

    public void mostrarNombreArea(String msg);

    public void cargarSectores(Sector[] sectores);
}
