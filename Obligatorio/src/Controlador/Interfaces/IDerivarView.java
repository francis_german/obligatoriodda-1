package Controlador.Interfaces;

import Modelo.Area;
import Modelo.Puesto;
import Modelo.Sector;

public interface IDerivarView {

    //public void preguntar(Atencion a);

    public void cargarAreas(Area[] areas);

    public void cargarSectores(Sector[] sectores);
    
    public void mostrarDestino(Puesto p);
    
    public void cerrar();
    
    public void marcarEsperando(boolean a);
    
    public void mensajeError(String msg);
    
    public void mensajeInfo(String msg);
    
    public Sector sectorSeleccionado();
    
   

}
