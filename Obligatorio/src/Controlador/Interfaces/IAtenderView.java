/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Interfaces;

import Modelo.Atencion;
import Modelo.Puesto;

/**
 *
 * @author Francis
 */
public interface IAtenderView {

    public void mostrarAtencion(Atencion a);

    public void mostrarPuestoVacio();

    public void nuevaDerivacion(int cant);

    public void cargarInfo(String msgTitulo, String msgArea);

    public void activarControles();

    public void desactivarControles();

    public void mensajeError(String msg);

    public void actualizarCantDerivados(int cant);

    public void abrirDerivacion(Puesto p);

    public void actualizarEstadisticas(Puesto p);

    public int preguntarDerivacion(String msg);
    
    public void cerrarPregunta();

}
