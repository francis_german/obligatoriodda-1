package Controlador.Interfaces;

import Modelo.Atencion;

public interface IMonitorView {

    public void actualizoMonitor(Atencion atencion, String nro);
    public void mostrarTitulo(String msg);
}
