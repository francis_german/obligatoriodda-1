
package Controlador.Interfaces;

import Modelo.Puesto;

public interface IIngresoTrabajadorView {
    public void cargarPuestosLibres(Puesto[] puestosLibres, String sector);
    public void mostrarError(int codigo, String mensaje);
}
