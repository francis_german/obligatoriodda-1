package Controlador;

import Controlador.Interfaces.IPedirNumeroView;
import Excepciones.AtencionException;
import Modelo.Area;
import Excepciones.ClienteException;
import Modelo.Fachada;
import Excepciones.NumeradorException;
import Modelo.Sector;

public class PedirNumeroController {

    private final Fachada modelo;
    private final IPedirNumeroView vista;
    private final Area area;

    public PedirNumeroController(IPedirNumeroView vista, int a) throws AtencionException {
        this.modelo = Fachada.getSistema();
        this.vista = vista;
        this.area = modelo.BuscarArea(a);
        this.vista.mostrarNombreArea(area.getNombre());
        this.vista.cargarSectores(area.getSectoresArray());

    }

    public String nombreArea() {
        return this.area.getNombre();
    }

    public void pedirNumero(int idSector, int nroCliente) {
        try {
            String nombreSector = modelo.buscarSector(idSector).getNombre();
            this.vista.mostrarNroAsignado(modelo.pidoNumero(idSector, nroCliente), nombreSector);
        } catch (NumeradorException | ClienteException e) {
            vista.mostrarError(e.getCodigo(), e.getMessage());
        }catch (AtencionException e) {
            vista.mostrarError(e.getCodigo(), e.getMessage());
        }
        
    }

}
