
package Controlador;

import Controlador.Interfaces.IVista;
import Excepciones.AtencionException;
import Modelo.Fachada;

public class AplicacionController {

    private final Fachada modelo;
    private final IVista vista;

    public AplicacionController(IVista vista) throws AtencionException {
        this.modelo = Fachada.getSistema();
        this.vista = vista;
    }
}
