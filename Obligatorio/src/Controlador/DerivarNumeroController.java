package Controlador;

import Controlador.Interfaces.IAtenderView;
import Controlador.Interfaces.IDerivarView;
import Excepciones.AtencionException;
import Excepciones.DerivacionException;
import Modelo.Area;
import Modelo.DTO.AtencionDTO;
import Modelo.Fachada;
import Modelo.Puesto;
import Modelo.Sector;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class DerivarNumeroController implements Observer {

    private final IDerivarView vista;
    private final Fachada modelo;
    private final Puesto puesto;
    private ArrayList<Puesto> puestosDerivacion;
    private final IAtenderView ventanaAtender;

    public DerivarNumeroController(IDerivarView vista, Puesto pOrigen, String desc, IAtenderView vAtender) throws AtencionException {
        this.vista = vista;
        this.modelo = Fachada.getSistema();
        this.puesto = pOrigen;
        this.modelo.agregarDescripcion(this.puesto, desc);
        this.puestosDerivacion = null;
        this.ventanaAtender = vAtender;
        this.cargarAreas();
    }

    @Override
    public void update(Observable o, Object arg) {

        AtencionDTO a = (AtencionDTO) arg;

        if (a != null) {

            if (a.getTipo() == AtencionDTO.Tipo.ATENCION) {
                return;
            }

            if (!a.isRespuesta()) {
                return;
            }

            this.vista.marcarEsperando(false);

            try{
                this.modelo.eliminarObserverDerivacion(this.puestosDerivacion.get(0), this);
            }
            catch(AtencionException e){
                
            }

            if (a.isAceptada()) {
                this.ventanaAtender.activarControles();
                this.vista.mensajeInfo("La derivacion fue aceptada por " + this.puestosDerivacion.get(0).getTrabajador().getNombreCompleto());
                this.vista.cerrar();
            } else {
                try {
                    if (this.puestosDerivacion.size() > 0) {
                        this.puestosDerivacion.remove(0);
                        if (this.puestosDerivacion.size() > 0) {
                            this.vista.mostrarDestino(this.puestosDerivacion.get(0));
                        }
                        this.derivar();
                    }
                } catch (AtencionException | DerivacionException ex) {
                    this.vista.mensajeError(ex.getMessage());
                }
            }
        }
    }

    public void cargarAreas() throws AtencionException {
        Area[] areas = this.modelo.pidoAreas();
        this.vista.cargarAreas(areas);
        this.cargarSectores(areas[0]);
    }

    public void cargarSectores(Area a) throws AtencionException {
        if (a.equals(this.puesto.getSector().getArea())) {
            this.vista.cargarSectores(this.modelo.pidoSectores(a, this.puesto.getSector()));
        } else {
            this.vista.cargarSectores(this.modelo.pidoTodosSectores(a));
        }
        Sector s = this.vista.sectorSeleccionado();
        this.puestosDerivacion(s);
    }

    public void derivar() throws AtencionException, DerivacionException {

        if (this.puestosDerivacion != null && this.puestosDerivacion.isEmpty()) {
            throw new DerivacionException(0, "No hay puestos disponibles para derivar");
        }

        if (this.puestosDerivacion.size() == 1) {
            Puesto pDestino = this.puestosDerivacion.get(0);
            AtencionDTO dto = new AtencionDTO();
            dto.setAtencion(this.puesto.getAtencionActual());
            dto.setUltimoPuesto(true);
            this.modelo.recibirDerivacion(pDestino, dto);
            this.vista.mensajeInfo("La derivacion fue asiganada a " + this.puestosDerivacion.get(0).getTrabajador().getNombreCompleto());
            this.vista.cerrar();
            this.ventanaAtender.activarControles();
        } else if (this.puestosDerivacion.size() > 1) {

            Puesto pDestino = this.puestosDerivacion.get(0);
            AtencionDTO dto = new AtencionDTO();
            dto.setAtencion(this.puesto.getAtencionActual());

            this.modelo.agregarObserverAlPuesto(pDestino, this);
            this.modelo.preguntarDerivacion(pDestino, dto);
            this.vista.marcarEsperando(true);
        }
    }

    public void puestosDerivacion(Sector s) {
        try{
            this.puestosDerivacion = this.modelo.pidoPuestosParaDerivacion(s);
        }
        catch(AtencionException e){
            
        }
        Puesto pDestino = null;
        if (this.puestosDerivacion.size() > 0) {
            pDestino = this.puestosDerivacion.get(0);
        }
        this.vista.mostrarDestino(pDestino);
    }

    public void cerrar() {
        this.vista.cerrar();
        this.ventanaAtender.activarControles();
    }

}
