package Costos;

import Modelo.Atencion;

public class Exonerado extends Costo implements ICosto{

    public Exonerado(){
        super(3);
    }
    @Override
    public double calcularCosto(Atencion a) {
        return 0.00;
    }

    @Override
    public int codigo() {
        return this.getCodigo();
    }
    
}
