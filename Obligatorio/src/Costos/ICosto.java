package Costos;

import Modelo.Atencion;

public interface ICosto {

    public double calcularCosto(Atencion a);
    
    public int codigo();

}
