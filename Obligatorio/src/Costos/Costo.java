package Costos;

public abstract class Costo {
    private int codigo;
    private final int monto = 100;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
    public int getMonto(){
        return this.monto;
    }
    
    public Costo(int codigo){
        this.codigo = codigo;
    }
}
