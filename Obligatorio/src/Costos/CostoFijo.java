package Costos;

import Modelo.Atencion;

public class CostoFijo extends Costo implements ICosto {

    public CostoFijo(){
        super(1);
    }
    
    @Override
    public double calcularCosto(Atencion a) {
        return this.getMonto();
    }

    @Override
    public int codigo() {
        return this.getCodigo();
    }

}
