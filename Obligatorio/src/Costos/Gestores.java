package Costos;

import Modelo.Atencion;
import org.joda.time.DateTime;
import org.joda.time.Duration;

public class Gestores extends Costo implements ICosto {

    public Gestores(){
        super(4);
    }
    @Override
    public double calcularCosto(Atencion a) {
        DateTime inicio = new DateTime(a.getFchHraCreado());
        DateTime fin = new DateTime(a.getFchHraInicio());

        //Diferencia del tiempo creada la atencion al tiempo que se incicio
        Duration d = new Duration(inicio, fin);

        long segundos = d.getStandardSeconds();
        segundos = segundos > this.getMonto() ? this.getMonto() : segundos;

        return this.getMonto() - segundos;
    }

    @Override
    public int codigo() {
        return this.getCodigo();
    }

}
