package DTOs;

import Utilidades.Utilidades;
import java.util.Date;

public class AtencionDTO {

    private int numero;
    private String nombreCli;
    private Date fchSolicitado;
    private String nombreSector;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombreCli() {
        return nombreCli;
    }

    public void setNombreCli(String nombreCli) {
        this.nombreCli = nombreCli;
    }

    public Date getFchSolicitado() {
        return fchSolicitado;
    }

    public void setFchSolicitado(Date fchSolicitado) {
        this.fchSolicitado = fchSolicitado;
    }

    public String getNombreSector() {
        return nombreSector;
    }

    public void setNombreSector(String nombreSector) {
        this.nombreSector = nombreSector;
    }

    @Override
    public String toString() {
        return "<strong>Numero:</strong> " + numero + "<br>"
                + "<strong>Cliente:</strong> " + nombreCli + "<br>"
                + "<strong>Sector:</strong> " + nombreSector + "<br>"
                + "<strong>Fecha Solicitado:</strong> " + Utilidades.FechaToString(fchSolicitado);
    }

}
