package Vistas;

import Controlador.Interfaces.IPedirNumeroView;
import Controlador.PedirNumeroController;
import DTOs.AtencionDTO;
import DTOs.SectorDTO;
import Excepciones.AtencionException;
import Modelo.Atencion;
import Modelo.Sector;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PedirNumeroView extends VistaWeb implements IPedirNumeroView {

    private final PedirNumeroController controlador;

    public PedirNumeroView(HttpServletRequest request, HttpServletResponse response) throws AtencionException {
        super(request, response);
        //Area que se le pasa por parametro por URL
        int areaId = Integer.parseInt(request.getParameter("area"));
        this.controlador = new PedirNumeroController(this, areaId);
        this.processRequest();
    }

    @Override
    public void mostrarNroAsignado(Atencion a, String nombreSector) {

        AtencionDTO model = new AtencionDTO();
        model.setNumero(a.getNumero());
        model.setFchSolicitado(a.getFchHraCreado());
        model.setNombreCli(a.getCliente().getNombreCompleto());
        model.setNombreSector(nombreSector);

        this.request.setAttribute("resultado", model);
    }

    @Override
    public void mostrarError(int codigo, String mensaje) {
        this.request.setAttribute("resultado", mensaje);
    }

    @Override
    public void mostrarNombreArea(String msg) {
        this.request.setAttribute("nombreArea", msg);
    }

    @Override
    public void cargarSectores(Sector[] sectores) {
        SectorDTO[] sectoresDTO = new SectorDTO[sectores.length];
        for (int i = 0; i < sectores.length; i++) {
            SectorDTO s = new SectorDTO();
            s.setId(sectores[i].getId());
            s.setNombre(sectores[i].getNombre());
            sectoresDTO[i] = s;
        }
        this.request.setAttribute("sectores", sectoresDTO);
    }

    @Override
    public void get() {
        try {
            this.redirect("PedirNumero.jsp");
        } catch (IOException ex) {
            Logger.getLogger(PedirNumeroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void post() {

        int idsector = Integer.parseInt(this.request.getParameter("sector"));
        int idcliente = Integer.parseInt(this.request.getParameter("idcliente"));

        this.controlador.pedirNumero(idsector, idcliente);

        try {
            this.redirect("PedirNumero.jsp");
        } catch (IOException ex) {
            Logger.getLogger(PedirNumeroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
