package Vistas;

import Controlador.Interfaces.IMonitorView;
import Controlador.MonitorViewController;
import Excepciones.AtencionException;
import Modelo.Atencion;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MonitorView extends VistaWeb implements IMonitorView {

    private final MonitorViewController controlador;

    public MonitorView(HttpServletRequest request, HttpServletResponse response) throws AtencionException, IOException {
        super(request, response);
        //Area que se le pasa por parametro por URL
        int areaId = Integer.parseInt(request.getParameter("area"));
        this.controlador = new MonitorViewController(this, areaId);
        this.processRequest();
    }

    @Override
    public void actualizoMonitor(Atencion atencion, String nro) {
        EnvioMonitorEvent e = EnvioMonitorEvent.ObtenerInstancia(request, response);
        String sector = atencion.getPuesto().getSector().getNombre();
        String puesto = atencion.getPuesto().getNroPuesto() + "";
        //String ret = "Sector: " + sector + " Puesto: " + puesto + " Numero: " + nro;
        String ret = sector + "|" + puesto + "|" + nro;
        e.enviar("nuevaAtencion", ret);
    }

    @Override
    public void mostrarTitulo(String msg) {
        this.request.setAttribute("titulo", msg);
    }

    @Override
    public void get() {
        try {
            this.redirect("Monitor.jsp");
        } catch (IOException ex) {
            Logger.getLogger(MonitorView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void post() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
