package Vistas;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class VistaWeb {

    protected HttpServletResponse response;
    protected HttpServletRequest request;

    protected VistaWeb(HttpServletRequest request, HttpServletResponse response) {
        this.response = response;
        this.request = request;
    }

    protected void processRequest() {
        if (this.request.getMethod().equals("GET")) {
            this.get();
        } else if (this.request.getMethod().equals("POST")) {
            this.post();
        }
    }

    private void requestDispatcher(String jsp) {
        try {
            this.request.getRequestDispatcher(jsp).forward(this.request, this.response);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(PedirNumeroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public abstract void get();

    public abstract void post();

    protected void redirect(String url) throws IOException {
        this.requestDispatcher(url);
        this.response.sendRedirect(url);
    }

}
