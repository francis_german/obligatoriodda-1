package Vistas;

import Controlador.AppClientesController;
import Controlador.Interfaces.IAppClientes;
import DTOs.AreaDTO;
import Excepciones.AtencionException;
import Modelo.Area;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ClientesView extends VistaWeb implements IAppClientes {

    private final AppClientesController controlador;

    public ClientesView(HttpServletRequest request, HttpServletResponse response) throws AtencionException {
        super(request, response);
        this.controlador = new AppClientesController(this);
        this.processRequest();
    }

    @Override
    public void cargoAreas(Area[] areas) {

        AreaDTO[] model = new AreaDTO[areas.length];

        for (int i = 0; i < areas.length; i++) {
            AreaDTO a = new AreaDTO();
            a.setId(areas[i].getId());
            a.setNombre(areas[i].getNombre());
            model[i] = a;
        }
        //Le pasa los dto al request para que arme la tabla el jsp
        this.request.setAttribute("areas", model);
    }

    @Override
    public void get() {
        this.controlador.cargarListaAreas();
        try {
            this.redirect("Clientes.jsp");
        } catch (IOException ex) {
            Logger.getLogger(PedirNumeroView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void post() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
