package Vistas;

import Controlador.AppClientesController;
import Controlador.Interfaces.IAppClientes;
import DTOs.AreaDTO;
import Excepciones.AtencionException;
import Modelo.Area;
import VistaSwing.AppTrabajadores;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class InicioView extends VistaWeb {


    public InicioView(HttpServletRequest request, HttpServletResponse response) throws AtencionException {
        super(request, response);
        this.processRequest();
    }

    @Override
    public void get() {
        try {
            this.redirect("Inicio.jsp");
        } catch (IOException ex) {
            Logger.getLogger(InicioView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void post() {
        try {
            this.redirect("Inicio.jsp");
        } catch (IOException ex) {
            Logger.getLogger(InicioView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
