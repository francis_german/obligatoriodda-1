package Vistas;

import Controlador.AppMonitoreoController;
import Controlador.Interfaces.IAppMonitoreo;
import DTOs.AreaDTO;
import Excepciones.AtencionException;
import Modelo.Area;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MonitoresView extends VistaWeb implements IAppMonitoreo {

    private final AppMonitoreoController controlador;

    public MonitoresView(HttpServletRequest request, HttpServletResponse response) throws AtencionException {
        super(request, response);
        this.controlador = new AppMonitoreoController(this);
        this.processRequest();
    }

    @Override
    public void cargoAreas(Area[] areas) {
        AreaDTO[] model = new AreaDTO[areas.length];

        for (int i = 0; i < areas.length; i++) {
            AreaDTO a = new AreaDTO();
            a.setId(areas[i].getId());
            a.setNombre(areas[i].getNombre());
            model[i] = a;
        }
        //Le pasa los dto al request para que arme la tabla el jsp
        this.request.setAttribute("areas", model);
    }

    @Override
    public void get() {
        try {
            this.controlador.cargarListaAreas();
            this.redirect("Monitores.jsp");
        } catch (IOException ex) {
            Logger.getLogger(MonitoresView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void post() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
