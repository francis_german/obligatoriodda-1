package Vistas;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class EnvioMonitorEvent {

    public static void Conectar(HttpServletRequest request, HttpServletResponse response) {
        EnvioMonitorEvent.ObtenerInstancia(request, response);
    }

    private AsyncContext contexto;
    private PrintWriter out;
    private HttpServletResponse response;
    private HttpServletRequest request;
    private static EnvioMonitorEvent instancia;

    private EnvioMonitorEvent(HttpServletRequest pRequest, HttpServletResponse pResponse) throws IOException {
        response = pResponse;
        request = pRequest;
        conectarSSE(request);
    }

    public static EnvioMonitorEvent ObtenerInstancia(HttpServletRequest pRequest, HttpServletResponse pResponse) {
        if (instancia == null) {
            try {
                instancia = new EnvioMonitorEvent(pRequest, pResponse);
            } catch (IOException ex) {
                Logger.getLogger(EnvioMonitorEvent.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            instancia.response = pResponse;
            instancia.request = pRequest;
        }
        return instancia;
    }

    //SIEMPRE IGUAL...
    public void conectarSSE(HttpServletRequest request) throws IOException {
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        contexto = request.startAsync();
        request = (HttpServletRequest) contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
    }

    public void enviar(String evento, String dato) {
        if (instancia != null) {
            out.write("event: " + evento + "\n");
            dato = dato.replace("\n", "");
            out.write("data: " + dato + "\n\n");
            if (out.checkError()) {//checkError llama a flush, si da false eNvio bien
                cerrar();
            }
        }
    }

    public void cerrar() {
        contexto.complete();
    }

}
