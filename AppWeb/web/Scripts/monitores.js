$(document).ready(inicio);

function inicio() {
    $(".abrir").click(abrir);
}
function abrir() {
    var id = $(this).data("id");
    var cantLineas = $("#txt_" + id).val();
    if (!cantLineas) {
        mensaje("Debe ingresar cantidad de lineas");
    } else {
        window.location.href = "/dda/Monitor?area=" + id + "&lineas=" + cantLineas + "";
    }
}
function mensaje(msg) {
    var icono = "<i class='fas fa-exclamation-circle'></i> ";
    $("#mensaje").html(icono + msg);
    $("#mensaje").addClass("alert alert-info");
}
