$(document).ready(inicio);

var atenciones = [];
var cantLineas;
var vistaWeb;

function inicio() {

    cantLineas = getUrlParameter("lineas");
    update();

}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}
function update() {
    vistaWeb = new EventSource("EnvioMonitor");

    vistaWeb.onError = function () {
        alert('Se perdio la conexion');
        vistaWeb.close();
    };

    vistaWeb.addEventListener("nuevaAtencion", function (evento) {
        agregarAtencion(evento.data);
        mostrarAtenciones();
    }, false);
}
function agregarAtencion(atencion) {
    atenciones.push(atencion);
}
function mostrarAtenciones() {
    var res = "";
    res = "<table><tr style=\"border-bottom: thin solid\"><th width='200'>Sector</th><th width='100' style=\"text-align:center\">Puesto</th><th width='100' style=\"text-align:center\">Nro</th></tr>";
    for (var i = atenciones.length - 1; i >= atenciones.length - cantLineas; i--) {
        if (atenciones[i]) {
            //res += atenciones[i] + "<br>";
            var datos = atenciones[i].split("|");
            res = res + "<tr style=\"border-bottom: thin solid\"><td>" + datos[0] + "</td><td style=\"text-align:center\">" + datos[1] + "</td><td style=\"text-align:center\">" + datos[2] + "</td></tr>";
        }
    }
    res = res + "</table>";
    $("#resultado").html(res);
}
