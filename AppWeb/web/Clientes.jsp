<%@page import="DTOs.AreaDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Web Clientes | Sistema Atencion al Publico </title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">        
    </head>
    <body>

        <div class="container-fuild">

            <div class="row">
                <div class="col-12">
                    <h1>Aplicacion Clientes</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">


                    <% AreaDTO[] areas = (AreaDTO[]) request.getAttribute("areas"); %>

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>Nombre</th>
                                <th>Abrir</th>
                            </tr>
                            <% for (AreaDTO a : areas) {%>
                            <tr>
                                <td><%=a.getNombre()%></td>
                                <td><a href="/dda/PedirNumero?area=<%=a.getId()%>" target="_blank" rel="noopener noreferrer"><i class="fas fa-external-link-square-alt"></i> Abrir</a></td>
                            </tr>
                            <%}%>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
