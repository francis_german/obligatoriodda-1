
<%@page import="DTOs.SectorDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Solicitar Numero area <%= request.getAttribute("nombreArea")%> | Sistema de Atencion al Publico</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>
    <body>
        <div class="container-fuild">

            <div class="row">
                <div class="col-12">
                    <h1>Solicitar Numero para area <%= request.getAttribute("nombreArea")%></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <form method="POST" class="form">
                        <div class="form-group row">
                            <strong class="col-2"><i class="fas fa-building"></i> Sector:</strong>
                            <div class="col-10">
                                <select name="sector" required class="form-control">
                                    <option value="">Elija un sector...</option>
                                    <% SectorDTO[] sectores = (SectorDTO[]) request.getAttribute("sectores"); %>
                                    <% for (SectorDTO s : sectores) {%>
                                    <option value="<%= s.getId()%>"><%= s.getNombre()%></option>
                                    <% }%>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <strong class="col-2"><i class="fas fa-user"></i> Id Cliente</strong>
                            <div class="col-10">
                                <input type="number" class="form-control" placeholder="Numero de cliente..." required name="idcliente">
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="submit" value="Pedir Numero" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <% if (request.getMethod().equals("POST")) {%>
        <!-- Modal -->
        <div class="modal fade" id="modal_res" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-file-alt"></i> Resultado</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><%= request.getAttribute("resultado")%></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--Abre el modal con el resultado...-->
        <script>
            $('#modal_res').modal('show');
        </script>
        <%}%>
    </body>
</html>
