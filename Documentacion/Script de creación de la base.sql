-- create database ObligatorioDDA;

use ObligatorioDDA;

 drop table Numeros;
 drop table Atenciones;
 drop table Puestos;
 drop table Trabajadores;
 drop table Sectores;
 drop table Areas;
 drop table Clientes;

create table Numeros(
					  tipo varchar(20) primary key,
                      ultNro int
					);

create table Areas(
					idArea int primary key,
                    nombre varchar(50) not null unique
				  );
                  
create table Sectores(
					   idSector int primary key,
                       idArea int not null,
                       nombre varchar(50) not null,
                       ultNroPuesto int,
                       ultNroAtencion int,
                       foreign key SectorArea(idArea) references Areas(idArea)
					 );
                     
create table Trabajadores(
						   idTrabajador int primary key,
                           nombre varchar(50) not null,
                           documento varchar(10) not null unique,
                           pass varchar(20) not null,
                           idSector int not null,
                           foreign key TrabajadorSector(idSector) references Sectores(idSector)
						 );
                         
create table Puestos(
					 idPuesto int primary key,
					 nroPuesto int not null,
                     idSector int not null,
                     idTrabajador int null,
                     atencionActual int null,
                     nrosAsignados int null,
                     tiempo time,
                     foreign key PuestoSector(idSector) references Sectores(idSector),
                     foreign key PuestoTrabajador(idTrabajador) references Trabajadores(idTrabajador),
                     unique key(nroPuesto,idSector)
					);
                    
create table Clientes(
					  idCliente int primary key,
                      nombre varchar(50) not null,
                      email varchar(50) not null ,
                      documento varchar(10) not null unique
					 );
                         
create table Atenciones(
						idAtencion int primary key,
                        idCliente int not null,
                        numero int not null,
                        fchHraCreado datetime not null,
                        fchHraInicio datetime,
                        fchHraFin datetime,
                        idPuesto int,
                        idTrabInicio int,
                        idTrabFin int,
                        descripcion varchar(100),
                        estado varchar(20),
                        foreign key AtencionCliente(idCliente) references Clientes(idCliente),
                        foreign key AtencionPuesto(idPuesto) references Puestos(idPuesto),
                        foreign key AtencionTrabInicio(idTrabInicio) references Trabajadores(idTrabajador),
                        foreign key AtencionTrabFin(idTrabFin) references Trabajadores(idTrabajador)
					   );
                       
insert into Numeros values ('Area', 1);
insert into Numeros values ('Sector', 1);
insert into Numeros values ('Trabajador', 1);
insert into Numeros values ('Puesto', 1);
insert into Numeros values ('Cliente', 1);
insert into Numeros values ('Atencion', 1);